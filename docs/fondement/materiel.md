---
title: Architecture matérielle
icon: material/numeric-1-circle
---

# Architecture matérielle des ordinateurs {:#hardware}


!!! progra "Programme"
    ## Programme {.hidden}  
    Exprimer un algorithme dans un langage de programmation a pour but de le rendre
    exécutable par une machine dans un contexte donné. La découverte de l’architecture des machines et de leur système d’exploitation constitue une étape importante.  
    Les circuits électroniques sont au cœur de toutes les machines informatiques. Les réseaux permettent de transmettre l’information entre machines. Les systèmes d’exploitation gèrent et optimisent l’ensemble des fonctions de la machine, de l’exécution des programmes aux entrées-sorties et à la gestion d’énergie.  
    On étudie aussi le rôle des capteurs et actionneurs dans les entrées-sorties clavier, interfaces graphiques et tactiles, dispositifs de mesure physique, commandes de machines, etc.

    |Notions|Compétences|Remarques|
    |--|--|--|
    Modèle d’architecture séquentielle (von Neumann) | Distinguer les rôles et les caractéristiques des différents constituants d’une machine. | La présentation se limite aux concepts généraux.<br> On distingue les architectures monoprocesseur et les architectures multiprocesseur.<br> Des activités débranchées sont proposées.<br> Les circuits combinatoires réalisent des fonctions booléennes.

!!! histoire "Histoire de l'architecture de von Neumann"
    ## Histoire {.hidden}  
    L'informatique revient donc à réaliser une suite d'**instructions** (combinaisons de portes logiques) sur un certain nombre de **données** binaires. Reste à déterminer comment organiser ces circuits logiques pour concevoir des ordinateurs... En **1945**, le développement du projet [EDVAC](https://fr.wikipedia.org/wiki/Electronic_Discrete_Variable_Automatic_Computer) amène  John von **Neumann** (mathématicien et physicien américano-hongrois 1903-1957) a imaginé l'utilisation d'une **structure de stockage (mémoire)** pour stocker à la fois les données et les instructions. Ce modèle repose sur l'idée d'un ordinateur composé de quatre composants principaux : l'unité de traitement central (CPU), la mémoire, l'unité de contrôle et les entrées/sorties. L'architecture de von Neumann a révolutionné le domaine de l'informatique en introduisant la notion de programme enregistré, où les instructions et les données sont stockées dans la même mémoire, ce qui permet une plus grande flexibilité et efficacité dans l'exécution des tâches. Ce modèle a jeté les bases de la conception des ordinateurs, a influencé de nombreuses autres architectures informatiques, mais reste le modèle central de l'informatique moderne.

<style>
svg.neumann {
    width: 100%;
}
.pointer-cursor {
  cursor: pointer;
}
/* The Modal (background) */
.modal {
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */

}
/* Modal Content */
.modal-content {
  background-color: var(--md-default-bg-color);
  /*color: var(--md-admonition-fg-color);*/
  font-size: 0.8rem;
  margin: 15% auto; /* 15% from the top and centered */
  padding: 20px;
  border: 1px solid #888;
  width: 30%; /* Could be more or less, depending on screen size */
}
/* The Close Button */
.close {
  color: #aaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}
.close:hover,
.close:focus {
  color: red;
  text-decoration: none;
  cursor: pointer;
}
</style>
??? def "Principe de l'architecture de von Neumann"
    ## Principe {.hidden}  
    !!! tip "Cliquez sur les différentes partie du schéma ci-dessous pour plus d'information."  

    --8<-- 'docs/assets/images/neumann.svg'

    !!! info "Les flèches représentent les différentes lignes de communication reliant les composants entre eux, appelées **bus**."

<script>
const elements = document.querySelectorAll('.element');
elements.forEach(element => {
    let el = element.parentElement.parentElement.parentElement;
    let rect = el.previousElementSibling;
    el.addEventListener('mouseenter', function() {
        this.classList.add('pointer-cursor');
    });
    el.addEventListener('mouseleave', function() {
      this.classList.remove('pointer-cursor');
    });
   rect.addEventListener('mouseenter', function() {
        this.classList.add('pointer-cursor');
    });
   
   rect.addEventListener('mouseleave', function() {
      this.classList.remove('pointer-cursor');
    });
    
 function create_modal (){
        // Create the modal
        const modal = document.createElement("div");
        modal.classList.add("modal");

        // Create the modal content
        const modalContent = document.createElement("div");
        modalContent.classList.add("modal-content");

        // Create the close button
        const closeBtn = document.createElement("span");
        closeBtn.classList.add("close");
        closeBtn.innerHTML = "&times;";
        // Add the close button to the modal content
        modalContent.appendChild(closeBtn);
        // Add the description to the modal content
        const description = document.createElement("p");
        description.innerHTML = el.querySelector('.description').innerHTML;

        modalContent.appendChild(description);

        // Add the modal content to the modal
        modal.appendChild(modalContent);

        // Add the modal to the document body
        document.body.appendChild(modal);

        // Close the modal when the close button is clicked
        closeBtn.addEventListener('click', function() {
            modal.remove();
        });

// Close the modal when clicked outside the modal content
        window.addEventListener('click', function(event) {
            if (event.target == modal) {
                modal.remove();
            }
        });
 }
 // Add click event listener
    el.addEventListener('click', create_modal)
    rect.addEventListener('click', create_modal)
});

</script>


??? def "Améliorations de l'architecture de von Neumann"
    ##  Améliorations {.hidden}

    ??? note "Pérenniser la mémoire"
        ### Pérenniser la mémoire {.hidden}
        La **mémoire vive** (**RAM** , pour Random Access Memory) de l'ordinateur a besoin d'être alimentée en permanence pour garder les données. À chaque extinction de l'ordinateur, elle est perdue : on la qualifie de mémoire volatile. Mais c'est dans la mémoire vive qu'est chargé le système d'exploitation de l'ordinateur (Windows, Linux, etc) et que sont stockées provisoirement les données dont il a besoin lors de l'exécution de programmes (navigateur Web, outils bureautiques, etc.). C'est en quelque sorte la mémoire à court-terme de l'ordinateur.

        Pour résoudre le problème de non persistance de la RAM, on recourt à deux types de mémoires non volatiles :

        - La **mémoire morte** est une mémoire qui ne peut être que lue (**ROM** Read Only Memory) : elle contient le micrologiciel (firmware) de l'ordinateur (BIOS ou UEFI) qui est le programme qui se charge à chaque allumage de l'ordinateur, avant même le lancement du système d'exploitation.
        - La **mémoire de masse**. Pour stocker les données et les programmes, on ajoute un périphérique appelé mémoire de masse : un disque dur (HDD = Hard Disk Drive) et/ou une mémoire Flash (SSD = Solid State Drive). Cette mémoire est capable de stocker une grande quantité de données, mais à l'inconvénient d'être beaucoup moins rapide que la mémoire vive. C'est pour cela que lors du lancement d'un programme les données nécessaires à son exécution sont généralement transférées vers la RAM pour une exécution plus rapide.


    ??? note "Accélérer l'accès à la mémoire"
        ### Accélérer l'accès à la mémoire {.hidden}
        Les performances des processeurs augmentant, l'accès à la **RAM** peut être un frein à l'exécution des tâches du processeur.

        Pour remédier à ce goulot d'étranglement, les processeurs contiennent maintenant de la **mémoire cache**, une mémoire encore plus rapide que la RAM.

        Dans un ordinateur, il y a donc plusieurs niveaux de mémoire, leur capacité (liée à leur coût) étant inversement proportionnelle à leur vitesse.

        |Mémoire |	Temps d'accès |	Débit 	| Capacité|
        |:--:|:--:|:--:|:--:|  
        Registre |	1 ns 	| |≈ Kio
        Mémoire cache |	2−3 ns 	| |≈ Mio
        RAM |	5−60 ns 	| 1−20 Gio/s |	≈ Gio
        Disque dur |	3−20 ms 	| 10−320 Mio/s |	≈ Tio

        !!! note "Rappel sur les multiples et sous-multiples d'unités"
            Symbole|n |µ|m||k|M|G|T|
            |:--|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
            Nom|nano|micro|milli|1|kilo|Méga|Giga|Téra
            Valeur|$10^{⁻9}$|$10^{⁻6}$|$10^{⁻3}$|1|$10^3$|$10^6$|$10^9$|$10^{12}$|

    ??? note "Multiplier les processeurs"
        ### Multiplier les processeurs {.hidden}

        Pendant des années, pour augmenter les performances des ordinateurs, les constructeurs augmentaient la **fréquence d'horloge des microprocesseurs** : la fréquence d'horloge d'un microprocesseur est liée à sa capacité d'exécuter un nombre plus ou moins important d'instructions machines par seconde. Plus la fréquence d'horloge du CPU est élevée, plus ce CPU est capable d'exécuter un grand nombre d'instructions machines par seconde 

        ![](!!images/clockspeeds_jpg){.center}

        A partir de 2006, la fréquence d'horloge a cessé d'augmenter du fait d'une contrainte physique : plus on augmente la fréquence d'horloge d'un CPU, plus ce dernier **chauffe**. Il est alors difficile de refroidir le CPU, les constructeurs de microprocesseurs (Intel, AMD, Apple..) ont donc arrêté la course à l'augmentation de la fréquence d'horloge, ils ont décidé d'adopter une nouvelle tactique : **augmenter le nombre de coeurs** présent sur un CPU.

        !!! info "Nombre de coeur d'un microprocesseur"

            Dans un microprocesseur, un coeur est principalement composé : d'une **UAL**, de **registres** (R0, R1...) et d'une **unité de commande**. Il est donc capable d'exécuter des programmes de façon autonome. 

            La miniaturisation des techniques de gravage des microprocesseurs, a permis à partir de 2006 de graver plusieurs coeurs sur une même puce. Aujourd'hui (en 2023), le dernier processeur d'intel i9-13900K est constitué de **24 coeurs** ! Même les smartphones possèdent des microprocesseurs multicoeurs : le Snapdragon 845 est octacore (8 coeurs).

        !!! warning "Attention"
            Augmenter le nombre de coeurs ne suffit pas, il faut que les applications soient développées pour pouvoir utiliser plusieurs coeurs en simultané (multi-threading) De plus, les différents coeurs du CPU doivent se "partager" l'accès à la mémoire vive : quand un coeur travaille sur une certaine zone de la RAM, cette même zone n'est pas accessible aux autres coeurs, ce qui, bien évidemment va brider les performances. Il en va de même pour l'utilisation de la mémoire cache.

