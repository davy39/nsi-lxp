---
title: Mbot
---
# Programmation du robot Mbot {:#mbot}

![mbot](!!images/mbot_png)


## Utilisation avec Linux

- Pour pouvoir se connecter au Mbot, il faut  tout d'abord désinstaller brltty, un paquet de prise en charge des clavier en braille qui interfère les ports série.  
- Ensuite il faut ajouter notre utilisateur au groupe autorisé à se connecter au port série (groupe `dialout`.)
- Enfin, il vous faudra utiliser le navigateur `chromium` pour que la communication série puisse avoir lieu. Ça ne marche pas avec Firefox ne marche pas.

```shell title="Dans un terminal, entrer les lignes de commande suivantes"
sudo apt remove brltty 
sudo usermod -a -G dialout $USER
sudo apt install chromium-browser
```

- Redémarrer l'ordinateur pour que cela prenne effet.



## C'est parti !

- Documentation sur l'utilisation de l'interface [ici](https://capytale2.ac-paris.fr/wiki/doku.php?id=mbot)

- N'hésitez pas à cliquer sur le bouton :fontawesome-solid-expand: pour passer en plein écran.


<iframe style="width: 133%;max-width: 133%; height: 750px;; transform: scale(0.75); transform-origin: top left;" src="https://fr.vittascience.com/mBot/?toolbox=vittascience&mode=mixed&console=bottom&nocloud=1" width="100%" height="700px" allow="fullscreen *; microphone *; camera *; serial *; usb *" bis_size="{&quot;x&quot;:0,&quot;y&quot;:474,&quot;w&quot;:1250,&quot;h&quot;:879,&quot;abs_x&quot;:0,&quot;abs_y&quot;:474}"></iframe>

