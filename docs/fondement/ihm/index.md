---
title: Interface Homme-Machine
icon: material/numeric-1-circle
---
# Interface Homme-Machine {:#ihm}

!!! progra "Programme"
    On étudie aussi le rôle des capteurs et actionneurs dans les entrées-sorties clavier, interfaces graphiques et tactiles, dispositifs de mesure physique, commandes de machines, etc.
    
    |Notions|Compétences|Remarques|
    |--|--|--|
    Périphériques d’entrée et de sortie<br>Interface Homme-Machine (IHM) | Identifier le rôle des capteurs et actionneurs.<br>Réaliser par programmation une IHM répondant à un cahier des charges donné.| Les activités peuvent être développées sur des objets connectés, des systèmes embarqués ou robots.

!!! def "Définition d'une Interface Homme-Machine (IHM)"
    Une interface homme-machine, également connue sous le nom d'IHM, est un système qui permet la communication entre les utilisateurs et les ordinateurs, facilitant ainsi l'interaction et le contrôle des systèmes informatiques. Les IHM utilisent souvent des **microcontrôleurs**, des circuits intégrés comprenant un processeur, de la mémoire et des **interfaces d'entrée/sortie**, pour contrôler (claviers, souris, boutons...) et surveiller (écrans) les opérations des systèmes. Les **capteurs**, qui détectent et mesurent des grandeurs physiques telles que la température ou la pression, permettent de collecter des données sur l'environnement. Les **actionneurs**, quant à eux, agissent physiquement sur l'environnement en réponse à des signaux électriques provenant du microcontrôleur, permettant ainsi de réaliser des actions précises en temps réel.

!!! abstract "Contenu de ce chapitre"
    Deux dispositifs sont présentés dans cette partie :

    - [Le robot Mbot](--mbot) : clé en main mais programmé avec arduino (langage C++ assez différent de Python), une interface de block est donc proposée.
    - [Circuitpython](--circuitpy) : permet de contrôler capteurs et actionneurs en Python. Un peut d'électronique sera nécessaire pour construire nos prototypes.