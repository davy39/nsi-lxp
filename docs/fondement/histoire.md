---
title: Histoire
icon: material/numeric-1-circle
---
# Histoire de l'informatique {:#histoire}

!!! progra "Programme"
    ## Programme {.hidden}
    Comme toute connaissance scientifique et technique, les concepts de l’informatique ont une histoire et ont été forgés par des personnes.

    Les algorithmes sont présents dès l’Antiquité, les machines à calculer apparaissent progressivement au XVIIe siècle, les sciences de l’information sont fondées au XIXe siècle, mais c’est en **1936** qu’apparaît le concept de **machine universelle**, capable d’exécuter tous les algorithmes, et que les notions de machine, algorithme, langage et information sont pensées comme un tout cohérent. Les premiers **ordinateurs** ont été construits en **1948** et leur puissance a ensuite évolué exponentiellement.

    Parallèlement, les ordinateurs se sont diversifiés dans leur taille, leur forme et leur emploi : téléphones, tablettes, montres connectées, ordinateurs personnels, serveurs, fermes de calcul, méga-ordinateurs. Le réseau internet, développé depuis 1969, relie aujourd’hui ordinateurs et objets connectés.  
    
    |Notions|Compétences|Remarques|
    |--|--|--| 
    Événements clés de l’histoire de l’informatique. | Situer dans le temps les principaux événements de l’histoire de l’informatique et leurs protagonistes.| Ces repères historiques seront construits au fur et à mesure de la présentation des concepts et techniques.

## Ressources
=== "Vidéos"
    {{tube_sci("Une histoire de l’architecture des ordinateurs", "92d2558d-a273-432e-9ec2-1b941aa4ed5c")}}
    {{youtube("En bref : de 1945 à nos jours", "dcN9QXxmRqk")}}
    {{youtube("Histoire de l'info 1/2","dJdiSN9q5QE")}}
    {{youtube("Histoire de l'info 2/2","NNxAKALRePo")}}
     
=== "Diaporama"
    Un diaporama à **2 dimensions** : utiliser les flèches haut/bas pour explorer un chapitre, et gauche/droite pour passer d'un chapitre à un autre.

    <iframe src="/fondement/histoire-diapo" frameborder="0" scrolling="auto" style="max-height: 100%; width: 100%; aspect-ratio: 3 / 2;"></iframe>


!!! note "Remarques"
    - Il ne s'agira pas de retenir toutes les dates et événements par coeur, mais de se représenter les grandes étapes du développement de l'informatique. Nous y reviendrons dans chacun des chapitres suivants.

    - POur les candidats libres passant les épreuves en première, il nous faudra identifier dans les [QCM de la banque](--sujets-bac) les quelques événements à connaître.  


## Quiz

{{quiz("Histoire_de_l_informatique_Premiere",20)}}

