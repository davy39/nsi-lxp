---
title: Fondements
icon: material/chip
---
# :material-chip: Les fondements de l'informatique :material-chip: {: #fondements}

!!! abstract "Présentation du chapitre"
    Ce chapitre présente les différents concepts et notions fondamentaux de l'informatique :
    
    1. Tout d'abord, nous aborderons l'[histoire de l'informatique](--histoire), en examinant les événements et les personnes clés qui ont contribué à son développement et à son évolution au fil du temps.
    
    - Ensuite, nous plongerons dans la [logique booléenne](--binaire), qui est un élément fondamental de la conception des circuits et des systèmes informatiques. La logique booléenne repose sur des concepts tels que les opérateurs logiques (ET, OU, NON) et les portes logiques, qui permettent de manipuler et de traiter l'information de manière binaire (`0` ou `1`).

    - Nous étudierons également l'[architecture matérielle des ordinateurs](--hardware), en examinant les différents composants qui les composent, tels que le processeur, la mémoire, les périphériques d'entrée/sortie, etc. Comprendre l'architecture matérielle est essentiel pour comprendre le fonctionnement interne des ordinateurs, et comment ils exécutent les **instructions**.

    - Nous verrons ensuite comment ces instructions sont codées et exécutées par un processeur, après que celles-ci aient été converties langage [assembleur puis en langage machine](--asm). Le langage machine est le langage de **bas niveau** compréhensible par un processeur, tandis que l'assembleur est une représentation **mnémonique** du langage machine, facilitant la programmation au niveau machine.

    - L'[interface homme-machine (IHM)](--ihm) concerne la manière dont les utilisateurs interagissent avec les systèmes informatiques. Cela peut inclure des éléments tels que les écrans, les claviers, les souris, les interfaces tactiles, les commandes vocales, etc. L'IHM est cruciale pour rendre les systèmes informatiques conviviaux et faciles à utiliser pour les utilisateurs finaux. Pour cela, nous initierons à l'utilisation de [CircuitPython](--circuitpy), version simplifiée du langage de programmation Python conçue pour les microcontrôleurs et les systèmes embarqués.
    
    - :material-alpha-t-circle: Les [systèmes sur puce (SoC)](--soc) sont des composants électroniques qui intègrent différents éléments matériels et logiciels sur une seule puce. Les SoC sont largement utilisés dans les appareils électroniques modernes tels que les smartphones, les tablettes, les appareils IoT, etc. Ils combinent généralement un processeur, des interfaces de communication, des périphériques d'entrée/sortie et d'autres composants sur une puce unique pour réduire la taille, la consommation d'énergie et les coûts. 