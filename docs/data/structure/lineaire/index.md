# Structure de données linéaires {: #struct-lineaire }

!!! progra "Programme"
    |Notions|Compétences|Remarques|
    |--|--|--| 
    Structures de données, interface et implémentation.<br> Listes, piles, files : structures linéaires. <br> Dictionnaires, index et clé. | Spécifier une structure de données par son interface.<br> Distinguer interface et implémentation. <br> Écrire plusieurs implémentations d'une même structure de données.<br> Distinguer des structures par le jeu des méthodes qui  caractérisent. <br> Choisir une structure de données adaptée à la situation à modéliser. <br> Distinguer la recherche d’une valeur dans une liste et dans un dictionnaire. | L’abstraction des structures de données est introduite après plusieurs implémentations d’une structure simple comme la file (avec un tableau ou avec deux piles). <br> On distingue les modes FIFO (first in first out) et LIFO (last in first out) des piles et des files.
    Algorithmes sur les arbres binaires et sur les arbres binaires de recherche. | Calculer la taille et la hauteur d’un arbre. <br> Parcourir un arbre de différentes façons (ordres infixe, préfixe ou suffixe ; ordre en largeur d’abord). <br>Rechercher une clé dans un arbre de recherche, insérer une clé. | Une structure de données récursive adaptée est utilisée. <br> L’exemple des arbres permet d’illustrer la programmation par classe. <br> La recherche dans un arbre de recherche équilibré est de coût logarithmique.

Une **structure linéaire** relie les données en **séquences**. C'est à dire qu'on peut numéroter les données, et que chaque élément a un rang.

Nous verrons dans ce chapitre l'exemple de la **LISTE**, dont la **PILE** et la **FILE** en sont des sous-exemple avec des opérations restreintes.

 
