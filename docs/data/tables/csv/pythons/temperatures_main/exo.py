#--- HDR ---#
url_fichier = "temperatures_2020.csv"
encodage = "utf-8"
from pyodide.http import pyfetch
import asyncio
async def download():
    response = await pyfetch(f"../{url_fichier}")
    if response.status == 200:
        with open(url_fichier, mode='w', encoding=encodage) as f:  
            f.write(await response.text())
loop = asyncio.get_event_loop()
loop.run_until_complete(download())

#--- HDR ---#
# Import
temperatures = []
with open(file="temperatures_2020.csv", mode="r", encoding="utf-8") as fichier:
    ...  # récupération des descripteurs
    for ligne in fichier:
        ligne_propre = ligne.strip()
        valeurs = ligne_propre.split(",")
        dico = {}
        for i in range(len(descripteurs)):
            dico[descripteurs[...]] = ...[i]
        temperatures.append(...)
