#--- HDR ---#
url_fichier = "temperatures_2020.csv"
encodage = "utf-8"
from pyodide.http import pyfetch
import asyncio
async def download():
    response = await pyfetch(f"../{url_fichier}")
    if response.status == 200:
        with open(url_fichier, mode='w', encoding=encodage) as f:  
            f.write(await response.text())
loop = asyncio.get_event_loop()
loop.run_until_complete(download())

#--- HDR ---#
# Import
import csv
with open("temperatures_2020.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=",")
    temperatures = [entree for entree in lecteur]

# Typage
for entree in temperatures:
    entree["jour"] = int(entree["jour"])
    entree["tmin"] = float(entree[...])
    entree[...] = ...(...[...])
    ...