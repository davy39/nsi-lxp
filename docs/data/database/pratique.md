---
title: Entraînement
---
# Mise en pratique du SQL

!!! info "Sql sur ce site"
    Pour connaître l'ensemble des tables de la base chargée dans les éditeurs SQL de ce site, il suffit d'écrire :

    ```sql
    SELECT * 
    FROM sqlite_master 
    WHERE TYPE ="table";
    ``` 

    et de valider.

## Base de données Prix Nobel

{{exo("Base de donnée des prix Nobel", 1)}}

    A l'aide de la cellule ci-dessous, répondre aux questions :

    {!{ sqlide titre="Base Prix Nobel" base="data/database/Documents/nobel.sqlite" sql="data/database/Documents/SqliteMaster.sql" }!}

    a) Combien de relation possède la base de données?

    b) Combien d'attributs possède la relation **nobel**?

    c) Quel est le type de l'attribut **annee**?

    ??? success "Réponse"

        a. Il y a une seule relation : `nobel`  
        b. Elle possède trois attributs : `annee`, `sujet`, `laureat`  
        c. L’attribut `annee` est du type Entier 

{{exo("Requêtes d'interrogation")}}
    !!! warning "Chaque requête devra se terminer par un `;` avant d'être exécutée."

    {!{ sqlide titre="Base Prix Nobel" base="data/database/Documents/nobel.sqlite" }!}

    1. Comment afficher le nom de tous les lauréats?
    2. Comment afficher le nom de toutes les disciplines **en évitant les doublons**?
    3. Quelle est la discipline de **Wilhelm Conrad Röntgen**?
    4. Dans quelle discipline **Paul Krugman** est-il devenu Prix Nobel?
    5. En quelle année **Albert Fert** a-t-il eu le prix Nobel?
    6. Quelle est l’année de distinction de **Pierre Curie**?
    7. Quelle est l’année de distinction et la matière de **Bertha von Suttner**?
    8. Quels sont les lauréats distingués au XXI e siècle?
    9. Quels sont les lauréats du prix Nobel de la Paix durant la deuxième guerre mondiale?
    10. Quels sont les lauréats distingués en Médecine en 1901 et 2001?
    11. Quels sont les lauréats des prix nobel de Physique et de Médecine en 2008?

    ??? success "Réponse"
        1.
        ```sql
        SELECT laureat
        FROM nobel;
        ```
        2.
        ```sql
        SELECT DISTINCT sujet
        FROM nobel;
        ```
        3.
        ```sql
        SELECT sujet
        FROM nobel 
        WHERE laureat LIKE "Wilhelm Conrad R%ntgen";
        ```
        4.
        ```sql
        SELECT sujet
        FROM nobel 
        WHERE laureat = "Paul Krugman";
        ```
        5.
        ```sql
        SELECT annee
        FROM nobel 
        WHERE laureat = "Albert Fert";
        ```
        6.
        ```sql
        SELECT annee
        FROM nobel 
        WHERE laureat = "Pierre Curie";
        ```
        7.
        ```sql
        SELECT annee, sujet
        FROM nobel 
        WHERE laureat = "Bertha von Suttner";
        ```
        8.
        ```sql
        SELECT laureat
        FROM nobel 
        WHERE annee >= 2000;
        ```
        9.
        ```sql
        SELECT laureat
        FROM nobel 
        WHERE annee >= 1939 AND annee <= 1945 AND sujet = "Paix";
        ```
        10.
        ```sql
        SELECT laureat
        FROM nobel 
        WHERE (annee = 1901 OR annee = 2001) AND sujet LIKE "M%decine";
        ```
        11.
        ```sql
        SELECT laureat
        FROM nobel 
        WHERE (sujet = "Physique" OR sujet LIKE "M%decine") AND annee = 2008;
        ```

{{exo("Requêtes d'agrégation")}}

    {!{ sqlide titre="Base Prix Nobel" base="data/database/Documents/nobel.sqlite" }!}

    1. Combien d’enregistrements au total comporte la relation?
    2. Combien de personnes ont reçu le prix Nobel de la paix?
    3. Combien de personnes ont reçu le prix Nobel de litérature?
    4. Combien de personnes ont reçu le prix Nobel de mathématiques?
    5. Combien de personnes ont reçu un prix Nobel en 1901?
    6. Combien de personnes ont reçu un prix Nobel de chimie en 1939?
    7. En quelle année a été décerné le premier prix Nobel d’économie?
    8. Combien de prix Nobel a reçu **Marie Curie**?
    9. Quels sont les prix lauréats, leur discipline et l’année de distinction de tous les prix Nobel contenant cohen dans leur nom (on ne fera pas de distinction de casse)?
    10. Combien y’a t’il eu de lauréats en Physique et en Chimie?
    11. Combien y’a t’il eu de lauréats de Médecine et de littérature en 2000?
    12. Nombre de lauréats diﬀérents parmi les prix nobels de la paix?

    ??? success "Réponse"
        1.
        ```sql
        SELECT count(*)
        FROM nobel;
        → 816 
        ```
        2.
        ```sql
        SELECT count(laureat)
        FROM nobel 
        WHERE sujet= "Paix";
        → 119
        ```
        3.
        ```sql
        SELECT count(laureat)
        FROM nobel 
        WHERE sujet="Literature";
        → 105
        ```
        4.
        ```sql
        SELECT count(laureat)
        FROM nobel 
        WHERE sujet="Mathematiques";
        → 0
        ```
        5.
        ```sql
        SELECT count(laureat)
        FROM nobel 
        WHERE annee = 1901;
        → 6
        ```
        6.
        ```sql
        SELECT count(laureat)
        FROM nobel 
        WHERE annee = 1939 AND sujet = "Chimie";
        → 2
        ```
        7.
        ```sql
        SELECT MIN(annee)
        FROM nobel 
        WHERE sujet = "Economie";
        → 1969
        ```
        8.
        ```sql
        SELECT count(annee)
        FROM nobel 
        WHERE laureat LIKE "%Marie Curie %";
        → 2
        ```
        9.
        ```sql
        SELECT laureat, sujet, annee
        FROM nobel 
        WHERE UPPER(laureat) LIKE "%COHEN %";
        → Claude Cohen-Tannoudji|Physique|1997
        Stanley Cohen|Médecine|1986
        ```
        10.
        ```sql
        SELECT count(DISTINCT laureat)
        FROM nobel 
        WHERE sujet = "Physique" OR sujet = "Chimie";
        → 335
        ```
        11.
        ```sql
        SELECT count(DISTINCT laureat)
        FROM nobel 
        WHERE (sujet = "Literature" OR sujet LIKE "M%decine") AND annee = 2000;
        → 4
        ```
        12.
        ```sql
        SELECT count(DISTINCT laureat)
        FROM nobel 
        WHERE sujet = "Paix";
        → 116
        ```

{{exo("Requêtes d'insertion")}}

    {!{ sqlide titre="Base Prix Nobel" base="data/database/Documents/nobel.sqlite" }!}

    1. En 2019, Esther Duﬂo a reçu le prix Nobel d’économie. Écrivez la requête permettant d’insérer cet enregistrement.
    2. Quelle requête permet de modiﬁer l’enregistrement précédent pour accoler le nom d’époux (Banerjee) après celui de Duﬂo?
    3. De nombreuses pétitions circulent pour retirer le prix Nobel à Aung San Suu Kyi. Quelle requête permettrait cela?

    ??? success "Réponse"
        1.
        ```sql
        INSERT INTO nobel
        VALUES (2019, "Economie", "Esther Duflo");
        ```
        2.
        ```sql
        UPDATE nobel
        SET laureat = "Esther Duflo Banerjee" 
        WHERE laureat = "Esther Duflo";
        ```
        3.
        ```sql
        DELETE FROM nobel
        WHERE laureat LIKE "Aung %";
        ```
## Base de données Collectivités

{{exo("Base de données collectivités")}}

    Dans la cellule ci-dessous, on a chargé la base de donnée `Collectivites.db`.

    {!{ sqlide titre="Base Collectivites" base="data/database/Documents/Collectivites.db" sql="data/database/Documents/SqliteMaster.sql" }!}

    1. Combien de relation possède la base de données?
    2. Réalisez un schéma relationnel de cette base de données, sous la forme graphique, en précisant pour chaque attribut son type et s'il doit impérativement être rempli.

    ??? success "Réponse"
        1. Deux relations : `Ville` et `Departement`
        
        2.

        |Ville|Departement|
        |:--:|:--:|
        |<u>idVille</u> (entier non nul)|<u>idDepartement (entier non nul)</u>|
        |nom (chaine non nulle)|numero (chaine non nulle)|
        |codePostal (entier non nul)|nom (chaine non nulle)|
        |nbHabitants (entier)||		
        |#idDepartement(entier non nul)||

{{exo("Collecte des informations")}}

    La base **Collectivites** est vide. Il faut la remplir. Pour cela, en vous aidant d'internet, complétez les tableaux suivants :

    |idCommune|Commune|Code Postal|Département|Nombre d'habitants|
    |--:|--:|--:|--:|--:|
    ||Rouen||||
    ||Dieppe||||
    ||Enverneu||||
    ||Le Neubourg|27110|Eure||
    ||Igoville||||


    |idDepartement|Département|Code d'immatriculation|
    |--:|--:|--:|
    ||Seine-Maritime||
    ||Eure||
    ||Calvados||


    ??? success "Réponse"
        |IdCommune|Commune|Code Postal|Département|Nombre d’habitants|
        |:--:|:--:|:--:|:--:|:--:|
        |1|Rouen|76000|Seine-Maritime|110169|
        |2|Dieppe|76200|Seine-Maritime|29080|
        |3|Envermeu|76630|Seine-Maritime|2097|
        |4|Le Neubourg|27110|Eure|4166|
        |5|Igoville|27460|Eure|1746|

        |IdDepartement|Département|Code d’immatriculation|
        |:--:|:--:|:--:|
        |1|Seine-Maritime|76|
        |2|Eure|27|
        |3|Calvados|14|

{{exo("Insertion des enregistrements")}}

    {!{ sqlide titre="Base Collectivites" base="data/database/Documents/Collectivites.db" }!}

    1. Insérez le département de Seine-Maritime.
    2. Insérez la commune de Rouen.
    3. Faîtes de même, en une seule requête, avec les communes de Dieppe et d’Envermeu.
    4. Insérez la commune d’Igoville.
    5. Insérez la commune du Neubourg.

    ??? success "Réponse"
        1.
        ```sql
        INSERT INTO Departement
        VALUES (1, "76", "Seine-Maritime");
        ```
        2.
        ```sql
        INSERT INTO Ville
        VALUES (1, "Rouen", 76000, 110169, 1);
        ```
        3.
        ```sql
        INSERT INTO Ville
        VALUES (2, "Dieppe", 76200, 29080, 1), (3, "Envermeu", 76630, 2097, 1);
        ```
        4.
        ```sql
        INSERT INTO Departement
        VALUES (2, "27", "Eure");
        INSERT INTO Ville 
        VALUES (5, "Igoville", 27460, 1746, 2);
        ```
        5.
        ```sql
        INSERT INTO Ville
        VALUES (4, "Le Neubourg", 27110, 4166, 2);
        ```

{{exo("Recherche d'informations")}}

    {!{ sqlide titre="Base Collectivites" base="data/database/Documents/Collectivites.db" }!}

    En recherchant éventuellement les informations manquantes, indiquez le code SQL permettant de répondre aux consignes suivantes :

    1. Trouville, Mézidon-Canon et Crèvecoeur-en-Auge sont des villes du Calvados.
    2. Le vrai nom de Trouville est en fait Trouville-Sur-Mer.
    3. Médizon-Canon et Crèvecoeur-en-Auge n’existent plus. Elles ont fusionné pour donner une nouvelle commune : Mézidon-Vallée-d’Auge.

    ??? success "Réponse"
        1.
        ```sql
        INSERT INTO Departement
        VALUES (3, "14", "Calvados");
        INSERT INTO Ville 
        VALUES (6, "Trouville", 14360, 4675, 3),
        (7, "Mezidon-Canon", 14270, 4838, 3),
        (8, "Crevecoeur-en-Auge", 14240, 480, 3);
        ```
        2.
        ```sql
        UPDATE Ville
        SET nom= "Trouville-sur-Mer"
        WHERE nom= "Trouville";
        ```
        3.
        ```sql
        INSERT INTO Ville
        VALUES (9, "Mezidon-Vallee-d’Auge", 14270, 9712) ;
        DELETE  FROM  Ville 
        WHERE  nom  =  "Mezidon-Canon"  OR  nom  =  "Crevecoeur-en-Auge";
        ```

{{exo("Vérification des enregistrements")}}

    {!{ sqlide titre="Base Collectivites" base="data/database/Documents/Collectivites.db" }!}

    Indiquez le code SQL permettant de répondre aux consignes suivantes :

    1. Combien il y a-t-il de départements différents enregistrés dans la base? (réponse : 3)
    2. Combien il y a-t-il de communes différentes enregistrées dans la base? (réponse : 7)
    3. Combien il y a-t-il de communes dans l’Eure enregistrées dans la base? (réponse : 2)
    4. Combien il y a-t-il de communes en Seine-Maritime enregistrées dans la base? (réponse : 3)
    5. Combien il y a-t-il de communes dans le Calvados enregistrées dans la base? (réponse : 2)

    ??? success "Réponse"
        1.
        ```sql
        SELECT count(*)
        FROM Departement;
        ```
        2.
        ```sql
        SELECT count(*)
        FROM Ville;
        ```
        3.
        ```sql
        SELECT count(*)
        FROM Ville 
        WHERE idDepartement = 2;
        ```
        4.
        ```sql
        SELECT count(*)
        FROM Ville 
        WHERE idDepartement = 1;
        ```
        5.
        ```sql
        SELECT count(*)
        FROM Ville 
        WHERE idDepartement = 3;
        ```

## Base de donnée Cinéma
{{exo("Base de donnée Cinéma")}}

    Dans la cellule ci-dessous, on a chargé la base de donnée `Cinema.sqlite`.

    {!{ sqlide titre="Base Cinema" base="data/database/Documents/Cinema.sqlite" }!}

    1. Combien de relations possède la base de données?
    2. Combien d’attributs possède la relation Artiste?
    3. Quelle est sa clé primaire?
    4. Combien d’attributs possède la relation Film?
    5. Quelle est sa clé primaire?
    6. Quelle est sa clé étrangère?
    7. Quelle est la référence de la clé étrangère.
    8. En procédant de la même façon pour toutes les tables, représentez le schéma relationnel sous forme graphique, en faisant bien ﬁgurer les relations entre les clé primaires et clés étrangères.

    ??? success "Réponse"
        1. 5 relations : Artiste, Cinema, Film, Role, Seance.
        2. Artiste à 3 attributs : nom, prenom, anneeNaissance.
        3. La clé primaire d’Artiste est nom.
        4. Film a 4 attributs : idFilm, titre, annee, nomRealisateur.
        5. La clé primaire de Film est idFilm.
        6. La clé étrangère de Film est nomRealisateur.
        7. La clé étrangère de Film fait référence à l’attribut nom de la relation Artiste.
        8. ![](images/Exercice35.png) 

{{exo("Requêtes simples")}}

    {!{ sqlide titre="Base Cinema" base="data/database/Documents/Cinema.sqlite" }!}

    Indiquez le code SQL permettant d’afficher :

    1. les titres des ﬁlms triés par ordre alphabétique.
    2. Les prénoms, noms et année de naissance des artites nés avant 1950.
    3. Les cinémas qui sont situés dans le 12ème arrondissement.
    4. Les artistes dont le nom commence par la lettre H (on utilisera la commande LIKE).
    5. Les acteurs dont on ignore la date de naissance (cela signiﬁe que la valeur n’existe pas).
    6. Le nombre de fois où Bruce Willis a joué le rôle de McLane (on utilisera la commande UPPER() pour s’affranchir de la casse).

    ??? success "Réponse"
        1.
        ```sql
        SELECT titre
        FROM Film 
        ORDER BY titre;
        ```
        2.
        ```sql
        SELECT prenom, nom, anneeNaissance
        FROM artiste 
        WHERE anneeNaissance <= 1950;
        ```
        3.
        ```sql
        SELECT nomCinema
        FROM Cinema 
        WHERE arrondissement = 12;
        ```
        4.
        ```sql
        SELECT prenom, nom
        FROM Artiste 
        WHERE nom LIKE "H %";
        ```
        5.
        ```sql
        SELECT prenom, nom
        FROM Artiste 
        WHERE anneeNaissance IS NULL;
        ```
        6.
        ```sql
        SELECT count(idFilm))
        FROM Role 
        WHERE UPPER(nomRole) = "MCLANE" AND UPPER(nomActeur) = "WILLIS";
        ```

{{exo("Requêtes avec jointure")}}

    On rapelle qu’une jointure consiste à rapprocher les clé primaire et étrangère de deux relations.

    La syntaxe est la suivante :

    ```sql
    SELECT  attribut 
    FROM  relation1 JOIN  relation2  
    ON  relation1.cle1  =  relation2.cle2;
    ```

    {!{ sqlide titre="Base Cinema" base="data/database/Documents/Cinema.sqlite" }!}

    Indiquez le code SQL permettant de répondre aux questions suivantes :

    1. Quel est le nom et le prénom de l’acteur qui a joué Tarzan (pensez à la commande UPPER()).
    2. Quelle est l’année de naissance du réalisateur de Reservoir Dogs?
    3. Quels sont les titres des ﬁlms dans lesquels a joué Woody Allen. Donnez aussi le rôle joué.
    4. Quels ﬁlms peut-on voir au cinéma Rex? (Attention aux doublons)
    5. Quels ﬁlms peut-on voir à 15h dans un cinéma parisien?
    6. Quels sont les cinémas (nom, adresse et arrondissement) qui diﬀusent des ﬁlms le matin.
    7. Quels sont les cinémas (nom, adresse et arrondissement) qui diﬀusent des ﬁlms le matin.<br>
    Indiquez aussi le titre du ﬁlm.
    8.Quels ﬁlms peut-on voir dans un cinéma du 12 e arrondissement? On donnera le titre du ﬁlm, le cinéma dans lequel il est joué et son adresse.
    9. Quels sont les nom et prénom des acteurs qui ont joué dans le ﬁlm Vertigo.
    10. Quel réalisateur a tourné dans ses propres ﬁlms? Donnez le nom, le rôle et le titre des ﬁlms.
    11. Où peut-on voir le ﬁlm Pulp Fiction? On donnera le nom, l’adresse du cinéma et numéro de la séance.
    12. Où peut-on voir un ﬁlm avec Clint Eastwood? On donnera le titre du ﬁlm, le nom et l’adresse du cinéma, ainsi que l’heure de début du ﬁlm.
    13. Quels sont les cinémas (nom, adresse et arrondissement) ayant une salle de plus de 150 places et passant un ﬁlm avec Bruce Willis?

    ??? success "Réponse"
        1.
        ```sql
        SELECT nom,prenom
        FROM Artiste
        JOIN Role ON Artiste.nom = Role.nomActeur 
        WHERE UPPER(nomRole) = "TARZAN";
        → Lambert Christophe
        ```
        2.
        ```sql
        SELECT anneeNaissance
        FROM Artiste
        JOIN Film ON Artiste.nom = Film.nomRealisateur 
        WHERE titre = "Reservoir Dogs";
        → 1948
        ```
        3.
        ```sql
        SELECT titre, nomRole
        FROM Film
        JOIN Role ON Film.idFilm = Role.idFilm 
        WHERE nomActeur = "Allen";
        → Manhattan, Davis
        → Annie Hall, Jonas
        ```
        4. ...


{{exo("Modification de la base cinéma")}}


    {!{ sqlide titre="Base Cinema" base="data/database/Documents/Cinema.sqlite" }!}

    1. On souhaite insérer un nouvel artiste. Il s’agit de Ridley Scott, né en 1937. Indiquez la requête SQL permettant de réaliser cette opération.
    2. Ridely Scott est le réalisateur du ﬁlm Blade Runner, sorti en 1982. Indiquez la requête SQL permettant de saisir ce nouvel enregistrement.
    3. Les acteurs principaux sont Harisson Ford (né en 1942) dans le rôle de Rick Deckard et Rutger Hauer (né en 1944) dans le rôle de Roy Batty. Indiquez la requête SQL permettant de saisir ces nouveaux enregistrements.
    4. Mark Hamill (né en 1951), Harisson Ford (né en 1942) et Carrie Fisher (née en 1956) sont les acteurs principaux du ﬁlm L’empire contre attaque réalisé en 1980 par Irvin Kershner (né en 1923). Indiquez les requêtes SQL permettant de saisir ces enregistrements.
    Indiquez l’ordre dans lequel il faut remplir les relations.
    5. Le réalisateur moldave Mevatlave Kraspeck est décédé brutalement hier à l’âge de 103 ans. Le Rex décide de lui rendre hommage en diﬀusant le ﬁlm Gant, Savon, Serviette et Ponge tourné en 1957 avec la star du cinéma muet Agathe Zeblouse (née en 1932) dans la
    salle 1 de 13h à 16h, le ﬁlm Shower on the Beach tourné en 1963 avec Camille Honnête (née en 1940) dans la salle 1 de 16h à 18h et Bains Moussants avec Jacques Célère (né en 1945 et décédé trop vite en 1969) tourné en 1967 dans la salle 2 de 15h à 18h. Indiquez
    les requêtes SQL permettant de modiﬁer les séances du cinéma Rex.

    ??? success "Réponse"
        1.
        ```sql
        INSERT INTO Artiste
        VALUES(‘Scott’, "Ridley", 1937);
        ```
        2.
        ```sql
        INSERT INTO Film
        VALUES(143, "Blade Runner", 1982, "Scott");
        ```
        3.
        ```sql
        INSERT INTO Artiste
        VALUES("Ford", "Harisson", 1942);
        INSERT INTO Artiste  
        VALUES("Hauer", "Rutger", 1944);
        INSERT INTO Role  
        VALUES("Rick Deckard", 143, "Ford");
        INSERT INTO Role  
        VALUES("Roy Batty", 143, "Hauer");
        ```
        4.
        ```sql
        INSERT INTO Artiste
        VALUES("Hamill", "Mark", 1951) 
        INSERT INTO Artiste  
        VALUES("Fisher", "Carrie", 1956);
        INSERT INTO Artiste  
        VALUES("Kershner", "Irvin", 1923);
        INSERT INTO Film 
        VALUES(144, "L empire contre attaque", 1980, "Kershner");
        INSERT INTO Role 
        VALUES("Luke Skywalker", 144, "Hamill");
        INSERT INTO Role 
        VALUES("Princesse Leia", 144, "Fisher");
        INSERT INTO Role 
        VALUES("Han Solo", 144, "Ford");
        ```
        5. ...


## Base de données Voyageurs

{{exo("Base Voyageurs")}}

    !!! info "Schéma de la base"
        **Voyageur** (<u>idVoyageur</u>, nom, prénom, ville, région)  
        **Séjour** (<u>idSéjour</u>, *#idVoyageur*, *#codeLogement*, début, fin)  
        **Logement** (<u>code</u>, nom, capacité, type, lieu)  
        **Activité** (<u>codeLogement, codeActivité</u>, description)  


    1. Exprimer les requêtes suivantes sans utiliser d'imbrication.

    ??? question "Nom des villes"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select ville from Voyageur
            ```

    ??? question "Nom des logements en Bretagne"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select nom from Logement 
            where lieu = 'Bretagne'
            ```

    ??? question "Nom des logements dont la capacité est inférieure à 20"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select nom from Logement 
            where capacité < 20
            ```

    ??? question "Description des activités de plongée"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select description from Activité 
            where codeActivité = 'Plongée'
            ```

    ??? question "Nom des logements avec piscine"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select nom from Logement as l, Activité as a 
            where l.code = a.codeLogement
            and a.codeActivité = 'Piscine'
            ```

    ??? question "Nom des voyageurs qui sont allés en Corse"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Logement as l, Séjour as s, Voyageur as v  
            where l.code = s.codeLogement
            and s.idVoyageur =v.idVoyageur
            and lieu= 'Corse'
            ```

    ??? question "Les voyageurs qui sont allés ailleurs qu'en Corse"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Logement as l, Séjour as s, Voyageur as v    
            where l.code = s.codeLogement
            and s.idVoyageur =v.idVoyageur
            and lieu != 'Corse'
            ```

    ??? question "Nom des logements visités par un auvergnat"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select l.nom as nomLogement, v.nom 
            from Logement as l, Séjour as s, Voyageur as v
            where l.code = s.codeLogement
            and s.idVoyageur =v.idVoyageur
            and région= 'Auvergne'
            ```

    ??? question "Nom des logements et des voyageurs situés dans la même région"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select l.nom as nomLogement, v.nom
            from Logement as l, Voyageur as v
            where région=lieu
            ```

    ??? question "Les paires de voyageurs (donner les noms) qui ont séjourné dans le même logement"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select distinct l.nom as nomLogement, v.nom as voyageur
            from Logement as l, Séjour as s1, Voyageur as v, Séjour as s2
            where l.code = s1.codeLogement
            and l.code = s2.codeLogement
            and s1.idVoyageur =v.idVoyageur
            and s2.idVoyageur =v.idVoyageur
            and s1.idSéjour != s2.idSéjour
            ```

    ??? question "Les voyageurs qui sont allés (au moins) deux fois dans le même logement"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select distinct l.nom as nomLogement, v.nom as voyageur
            from Logement as l, Séjour as s1, Voyageur as v, Séjour as s2
            where l.code = s1.codeLogement
            and l.code = s2.codeLogement
            and s1.idVoyageur =v.idVoyageur
            and s2.idVoyageur =v.idVoyageur
            and s1.idSéjour != s2.idSéjou
            ```

    ??? question "Les logements qui ont reçu (au moins) deux voyageurs différents"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select distinct l.nom as nomLogement, v1.nom as voyageur1, v2.nom as voyageur2
            from Logement as l, Séjour as s1, Voyageur as v1, Séjour as s2, Voyageur as v2
            where l.code = s1.codeLogement
            and  l.code = s2.codeLogement
            and  s1.idVoyageur =v1.idVoyageur
            and  s2.idVoyageur =v2.idVoyageur
            and  s1.idVoyageur != s2.idVoyageur
            ```

    2. Pour les requêtes suivantes, en revanche, vous avez droit à l'imbrication (il serait difficile de faire autrement).

    ??? question "Nom des voyageurs qui ne sont pas allés en Corse"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Voyageur as v
            where not exists (select ''
            from Logement as l, Séjour as s
            where l.code = s.codeLogement
            and s.idVoyageur =v.idVoyageur
            and lieu='Corse')
            ```
    ??? question "Noms des voyageurs qui ne vont qu'en Corse s'ils vont quelque part"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Voyageur as v
            where not exists (select  ''
            from Logement as l, Séjour as s
            where l.code = s.codeLogement
            and s.idVoyageur =v.idVoyageur
            and lieu != 'Corse')
            ```    
    ??? question "Nom des logements sans piscine"
    
        ??? success "Réponse"
            ```sql
            select l.nom
            fromLogement as l
            where not exists (select ''
            from Activité as a
            where l.code= a.codeLogement
            and a.codeActivité= 'Piscine')
            ```
    ??? question "Nom des voyageurs qui ne sont allés nulle part"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Voyageur as v
            where not exists (select ''
            from Séjour as s
            where s.idVoyageur =v.idVoyageur)
            ```
    ??? question "Les logements où personne n'est allé"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select l.nom
            from Logement as l
            where not exists (select ''  
            from Séjour as s
            where l.code =s.codeLogement)
            ```
    ??? question "Les voyageurs qui n'ont jamais eu l'occasion de faire de la plongée"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.prénom, v.nom
            from Voyageur as v
            where not exists (select ''
            from Séjour as s,  Activité as a
            where s.idVoyageur =v.idVoyageur
            and s.codeLogement=a.codeLogement
            and a.codeActivité='Plongée')
            ```     
    ??? question "Les voyageurs et les logements où ils n'ont jamais séjourné"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select v.nom as nomVoyageur, l.nom as nomLogement
            from Voyageur as v, Logement as l
            where not exists (  
                select ''
                from Séjour as s
                where s.codeLogement = l.code
                and s.idVoyageur =v.idVoyageur)
            ```

    ??? question "Les logements où tout le monde est allé"
        {!{ sqlide titre="Base Voyageurs" base="data/database/Documents/Voyageurs.db" }!}
        ??? success "Réponse"
            ```sql
            select l.nom
            from Logement as l
            where not exists (  select *
                from Voyageur as v
                where not exists (     
                    select * from Séjour as s
                    where s.idVoyageur = v.idVoyageur
                    and s.codeLogement = l.code
                )
            ```


## Base de données Films

{{exo("Recherches sur des films et acteurs")}}

    {!{sqlide base="data/database/Documents/Films.db" }!}

    ??? question "1 - Nom et année de naissance des artistes né́s avant 1950"
        ```sql
        select  * from Artiste 
        where annéeNaiss < 1950
        ```
    ??? question "2 - Titre de tous les drames"
        ```sql
        select titre from Film 
        where genre = 'Drame'
        ```
    ??? question "3 - Quels rôles a joué Bruce Willis"
        ```sql
        select nomRôle
        from Rôle as r, Artiste as a
        where idActeur =idArtiste and nom =   'Willis'
        ```
    ??? question "4 - Qui est le réalisateur de Memento"
        ```sql
        select prénom, nom
        from   Film f, Artiste a
        where  f.titre = 'Memento'
        and    f.idRéalisateur = a.idArtiste
        ```
    ??? question "5 - Qui a joué le rôle de Chewbacca?"
        ```sql
        select prénom, nom
        from Rôle as r, Artiste as a
        where idActeur =idArtiste
        and nomRôle = 'Chewbacca'
        ```
    ??? question "6 - Dans quels films Bruce Willis a-t-il joué́ le role de John McClane ?"
        ```sql
        select titre
        from Film as f, Rôle as r, Artiste as a
        where f.idFilm=r.idFilm
        and idActeur =idArtiste
        and nom =   'Willis' and nomRôle='John McClane'
        ```
    ??? question "7 - Nom des acteurs de 'Sueurs froides'"
        ```sql
        select prénom, nom, nomRôle
        from Film as f, Rôle as r, Artiste as a
        where  titre = 'Sueurs froides'
        and    f.idFilm = r.idFilm
        and    r.idActeur = a.idArtiste
        ```

    ??? question "8 - Films dont le réalisateur est Tim Burton, et l’un des acteurs Jonnhy Depp."
        ```sql
        select titre
        from   Film as f, Rôle as r, Artiste as a, Artiste as b
        where  f.idFilm = r.idFilm
        and  r.idActeur = a.idArtiste
        and    f.idRéalisateur = b.idArtiste
        and     b.nom='Burton'
        and    a.nom='Depp'
        ```
    ??? question "9 - Titre des films dans lesquels a joué́ Woody Allen. Donner aussi le rôle."
        ```sql
        select titre, nomRôle
        from   Film as f, Rôle as r, Artiste as a
        where  f.idFilm = r.idFilm
        and    r.idActeur = a.idArtiste
        and    a.nom='Allen'
        and    a.prénom='Woody'
        ```
    ??? question "10 - Quel metteur en scène a tourné́ dans ses propres films ? Donner le nom, le rôle et le titre des films."
        ```sql
        select prénom, nom, nomRôle, titre
        from   Rôle as r, Film as f, Artiste as a
        where  f.idFilm = r.idFilm
        and    f.idRéalisateur = a.idArtiste
        and    r.idActeur = a.idArtiste
        ```
    ??? question "11 - Titre des films de Quentin Tarantino dans lesquels il n’a pas joué"
        ```sql
        select titre
        from Film as f1, Artiste as s
        where f1.idRéalisateur=a.idArtiste
        and nom='Tarantino'
        and  not exists (select '' from Rôle r
        where  f.idFilm = r.idFilm
        and r.idActeur=a.idArtiste)
        ```
    ??? question "12 - Quel metteur en scène a tourné́ en tant qu’acteur ? Donner le nom, le rôle et le titre des films dans lesquels cet artiste a joué."
        ```sql
        select prénom, nom, nomRôle
        from   Rôle as r, Film as f1, Film as f2, Artiste as a
        where  f1.idFilm = r.idFilm
        and    f2.idRéalisateur = a.idArtiste
        and    r.idActeur = a.idArtiste
        ```
    ??? question "13 - Donnez les films de Hitchcock sans James Stewart"
        ```sql
        select titre from Film as f, Artiste as a
        where f.idRéalisateur = a.idArtiste
        and a.nom='Hitchcock'
        and not exists (select '' from Artiste as a2, Rôle as r
        where a2.idArtiste = r.idActeur
        and r.idFilm=f.idFilm
        and nom='Stewart')
        ```
    ??? question "14 - Dans quels films le réalisateur a-t-il le même pré́nom que l’un des interprètes ? (titre, nom du réalisateur, nom de l’interprète). Le réalisateur et l’interprète ne doivent pas être la même personne."
        ```sql
        select a1.prénom as prénom1, a1.nom as nom1, titre,
        a2.prénom as prénom2, a2.nom as nom2
        from   Rôle as r, Film as f, Artiste as a1, Artiste as a2
        where  f.idFilm = r.idFilm
        and    a1.idArtiste != a2.idArtiste
        and    f.idRéalisateur = a1.idArtiste
        and    r.idActeur = a2.idArtiste
        and    a1.prénom = a2.prénom
        ```
    ??? question "15 - Les films sans rôle"
        ```sql
        select titre
        from Film
        where idFilm not in  (select idFilm  from Rôle)
        ```

    ??? question "16 - Quels acteurs n’ont jamais réalisé de film ?"
        ```sql
        select prénom, nom
        from Artiste as a
        where not exists (select 'c' from Film as f
        where a.idArtiste = f.idRéalisateur)
        ```

    ??? question "17 - Quelle est la moyenne des notes de Memento"
        ```sql
        select avg (note) as moyenne
        from Film as f, Notation as n
        where f.idFilm=n.idFilm
        and f.titre='Memento'
        ```

    ??? question "18 - id, Nom et prénom des réalisateurs, et nombre de films qu’ils ont tournés."
        ```sql
        select idArtiste, nom, prénom, count(titre) as nbFilms
        from   Artiste as a, Film as f
        where   f.idRéalisateur = a.idArtiste
        group by  idArtiste
        ```
        
    ??? question "19 - Nom et prénom des réalisateurs qui ont tourné au moins deux films."
        ```sql
        select  nom, prénom
        from   Artiste as a, Film as f
        where f.idRéalisateur = a.idArtiste
        group by nom, prénom
        having count(*) > 1
        ```

    ??? question "20 - Quels films ont une moyenne des notes supérieure à 7"
        ```sql
        select titre, avg (note) as moyenne
        from Film as f, Notation as n
        where f.idFilm=n.idFilm
        group by f.idFilm
        having avg(note) > 7
        ```



## SQL Murder Mystery

{{exo("SQL Murder Mystery")}}

    Un meurtre  a été commis.  Comme vous  ne prenez jamais  de notes, vous  ne vous
    souvenez plus du nom du meurtrier. Vous  vous souvenez juste que le meurtre a eu
    lieu le 15 janvier 2018 dans la ville de SQL City.

    Vous disposez ensuite de la base de données schématisée ici :

    ![bdd de sql murder mystery](images/sqlmm.png)

    A vous de jouer :


    {!{ sqlide titre="Entrez ici vos requêtes pour mener l'enquête:" base="data/database/Documents/sql-murder-mystery.db" espace="bddmm"}!}


    Vous rentrerez ensuite votre solution ici :


    {!{ sqlide titre="Votre solution:" sql="data/database/Documents/sqlmm-sol.sql" espace="bddmm"}!}

    _Source:_ [SQL Murder Mystery](https://mystery.knightlab.com/)