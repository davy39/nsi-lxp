---
title: Les SGBD
---

# Systèmes de gestion de base de données (SGBD) {:#sgbd}

??? histoire "Un peu d'histoire"
    |||
    |:--:|:--:|
    | 1970 | Edgar F. Codd propose le modèle relationnel 
    |1960|développement de l’IMS (Information Management System) par la société IBM dans le cadre du programme Apollo.|
    |1979| création du premier SGBD Oracle|
    | 1986 | Microsoft lance SQL Server 
    |1986| Création de PostgreSQL |
    |1995| Première version de MySQL |
    |2000| Création de Sqlite
    |2009| Création de MariaDB suite au rachat de MySql par Orale


!!! def "Définition d'un SGBD"
    Un **Système de Gestion de Base de Données (SGBD)** est un outil permettant aux utilisateurs de structurer, d’insérer, de modiﬁer et de rechercher de manière eﬃcace des données au sein d’une
    grande quantité d’informations stockées sur des mémoires partagées.

    ![](images/tab9.JPG){: .center}

!!! def "Propriétés"

    Les SGBD permettent de gérer la lecture, l’écriture ou la modiﬁcation des informations contenues dans une base de données.

    - elles permettent de gérer les autorisations d’accès à une base de données. Il est en effet souvent nécessaire de contrôler les accès par exemple en permettant à l’utilisateur A de lire
    et d’écrire dans la base de données alors que l’utilisateur B aura uniquement la possibilité de lire les informations contenues dans cette même base de données. Les accès sont ainsi
    sécurisés.
    - les ﬁchiers des bases de données sont stockés sur des disques durs dans des ordinateurs, ces ordinateurs peuvent subir des pannes. Il est souvent nécessaire que l’accès aux informations
    contenues dans une base de données soit maintenu, même en cas de panne matérielle. Les bases de données sont donc dupliquées sur plusieurs ordinateurs aﬁn qu’en cas de panne d’un ordinateur A, un ordinateur B contenant une copie de la base de données présente dans A, puisse prendre le relais. Tout cela est très complexe à gérer, en eﬀet toute modiﬁcation de la base de données présente sur l’ordinateur A doit entraîner la même modiﬁcation de la base de données présente sur l’ordinateur B. Cette synchronisation entre A et B doit se faire le plus rapidement possible, il est fondamental d’avoir des copies parfaitement identiques en permanence. C’est aussi les SGBD qui assurent la maintenance des diﬀérentes copies de la base de données. On parle de persistance des données.
    - plusieurs personnes peuvent avoir besoin d’accéder aux informations contenues dans une base données en même temps. Cela peut parfois poser problème, notamment si les 2 personnes désirent modiﬁer la même donnée au même moment (on parle d’accès concurrent).

    Ces problèmes d’accès concurrent sont aussi gérés par les SGBD.

    ![](images/tab10.JPG){: .center}

    L’utilisation des SGBD explique en partie la supériorité de l’utilisation des bases de données sur des solutions plus simples à mettre en oeuvre; mais aussi beaucoup plus limitées comme les ﬁchiers au format CSV.

    L’interaction avec le SGBD se fait par l’intermédiaire de **requêtes** exprimées dans un langage devenu standard au ﬁl des temps : le [langage SQL (Structured Query Language)](--sql).


!!! note "A Retenir"

    Un système de gestion de base de données doit pouvoir :

    -  décrire les données, indépendamment des applications;
    -  manipuler les données : dire QUOI sans dire COMMENT;
    -  contrôler les données pour s’assurer de leur intégrité ou qu’elles vériﬁent les contraintes;
    -  partager les données entre plusieurs utilisateurs;
    -  sécuriser les données (reprise après panne, journalisation).