---
title: Bases de données
icon: material/alpha-t-circle
---
# Les bases de données {: #database }

!!! progra "Programme"
    |Notions|Compétences|Remarques|
    |--|--|--| 
    Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel. | Identifier les concepts définissant le modèle relationnel.|  Ces concepts permettent d’exprimer les contraintes d’intégrité (domaine, relation et référence).
    Base de données relationnelle.| Savoir distinguer la structure d’une base de données de son contenu.<br> Repérer des anomalies dans le schéma d’une base de données.| La structure est un ensemble de schémas relationnels qui respecte les contraintes du modèle relationnel.<br> Les anomalies peuvent être des redondances de données ou des anomalies d’insertion, de suppression, de mise à jour.<br> On privilégie la manipulation de données nombreuses et réalistes.
    Système de gestion de bases de données relationnelles.| Identifier les services rendus par un système de gestion de bases de données relationnelles : persistance des données, gestion des accès concurrents, efficacité de traitement des requêtes, sécurisation des accès.| Il s’agit de comprendre le rôle et les enjeux des différents services sans en détailler le fonctionnement.

## Introduction : les limites du traitement des données

Nous avons vu précédemment différents type de données structurées et leur implémentation en python. En particulier les [listes](--liste), les [tuples](--tuple) et les [dictionnaires](--dictionnaire) permettent de stocker et de manipuler des ensemble de données, en particulier sous la forme de [données en table](--table) qui peuvent être stockés dans des fichiers CSV. L'objectif de ces premières activités est à la fois de se remémorer l'utilisation de ces structures, mais aussi de mettre en évidence leurs limites lorsqu'il s'agit de traiter des données plus nombreuses et plus complexes.

{{ exo ("Indexation des ouvrages d'une bibliothèque", 1 )}}
    Voyons tout d'abord comment indexer les ouvrages d'une bibliothèque avec des dictionnaires en python.  
      
    {{jupy("Indexation des ouvrages d'une bibliothèque.ipynb")}}  {{jupy_cor("Indexation des ouvrages d'une bibliothèque - Correction.ipynb")}}  


{{ exo ("Manipuler des données avec une table") }}
    Voyons maintenant comment gérer notre bibliothèque à partir d'un fichier CSV et une table.  
      
    {{ jupy("Manipuler des données avec une table.ipynb") }}  {{ jupy_cor("Manipuler des données avec une table - Correction.ipynb") }}


!!! abstract "Au programme dans ce chapitre"
    - Nous verrons tout d'abord, d'un point de vue conceptuel, ce qu'est le [modèle relationnel](--relationnel), **modèle de données** qui organise les données sous forme **relations**. 
    - Nous verrons ensuite, dans la pratique, comment différents [Systèmes de Gestion de Bases de Données (SGBD)](--sgbd) permettent de stocker, gérer et manipuler des **bases de données**. 
    - Le langage [SQL (Structured Query Language)](--sql) sera introduit afin de communiquer avec les **bases de données relationnelles** et effectuer des **requêtes**.
    - Enfin la dernière partie nous permettra de nous entraîner à [effectuer des requêtes SQL](--pratique-sql). 