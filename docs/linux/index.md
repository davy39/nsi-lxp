---
title: Linux
icon: material/linux
---

# :material-linux: Systèmes d'exploitation :material-linux: {: #linux }
!!! progra "Programme"
    |Notions|Compétences|Remarques|
    |--|--|--|
    Systèmes d’exploitation.|Identifier les fonctions d’un système d’exploitation.<br>Utiliser les commandes de base en ligne de commande.<br>Gérer les droits et permissions d’accès aux fichiers. | Les différences entre systèmes d’exploitation libres et propriétaires sont évoquées. Les élèves utilisent un système d’exploitation libre. Il ne s’agit pas d’une étude théorique des systèmes d’exploitation.

!!! def "Définition : Système d'exploitation"
    Un **système d'exploitation** - Operating System (**OS**) - est un ensemble de logiciels qui permettent de faire fonctionner d'autres logiciels en exploitant les ressources proposées par un ordinateur (RAM, CPU, disques...). Les logiciels n'ont pas vraiment à gérer les ressources matérielles, le système d'exploitation s'en charge pour eux.

    {{tube_sci("L'OS expliqué en 3 minutes", "e9dacd7d-f44c-4479-a408-43fcd6ce7c60")}}


!!! abstract "Contenu de ce chapitre"
    - Chapitre 1: [Familles d'OS](--os)
        Découverte des différentes familles de systèmes d'exploitation tels que Windows, macOS et Linux. 

    - Chapitre 2: [Passer à Linux](--linux)
        Les différentes distributions de Linux, comment les tester et les installer.

    - Chapitre 3: [Commandes Shell](--commande)
        Introduction aux commandes Shell, utilisées avec Linux comme avec MacOS.

    - Chapitre 4: [Arborescence de dossiers](--arborescence)
        L'organisation arborescente des dossiers d'un OS de type Unix, et comment y naviguer.

    - Chapitre 5: [Gestion des permissions](--permission)
        Contrôler l'accès aux fichiers et aux dossiers de votre système d'exploitation. Attribuer les droits adéquats aux utilisateurs et aux groupes.

    - Chapitre 6: [Gestion des processus](--processus)
        Définition des processus d'un système d'exploitation. Comment les surveiller, mettre en pause et les arrêter.