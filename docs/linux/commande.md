---
title: Commandes Shell
icon: material/numeric-1-circle
---
# Les commandes sous Unix / Linux {:#commande}


!!! progra "Programme"
    |Notions|Compétences|Remarques|
    |--|--|--|
    Systèmes d’exploitation.|Utiliser les commandes de base en ligne de commande. | |



??? tool "Terminus"
    ## Terminus {.hidden}
    **Terminus** est un jeu qui permet d'apprendre les commandes de bases sous Linux.
    
    <iframe src="/assets/tools/Terminus" frameborder="0" scrolling="auto" style="height: 80vh; width: 100%;"></iframe>

??? def "Quelques commandes utiles"
    ## Quelques commandes utiles {.hidden}
    <iframe src="/linux/commande-diapo" frameborder="0" scrolling="auto" style="max-height: 100%; width: 100%; aspect-ratio: 3 / 2;"></iframe>