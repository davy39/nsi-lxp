---
title: Gestion des permissions
icon: material/numeric-1-circle
---
# La gestion des permissions dans le shell {:#permission}

!!! progra "Programme"
    |Notions|Compétences|Remarques|
    |--|--|--|
    Systèmes d’exploitation.|Gérer les droits et permissions d’accès aux fichiers. | |

??? def "Lecture des droits"
    Il est possible d'utiliser la commande `ls` avec l'option `-l` afin d'avoir des informations supplémentaires.  
    Nous sommes positionnés dans le répertoire `boulot` et nous avons exécuté la commande `ls -l` :
    ```shell-session
    kelian@mon-pc:~/Documents/boulot$ ls -l
    total 1
    -rw-r--r--  1   kelian    kelian    0   Feb 20 14:59 TODO.txt
    ```

    Lisons cette ligne de gauche à droite :  
    
    -  Première partie : ```-rw-r--r--```

        - le premier symbole `-` signifie que l'on a affaire à un fichier, dans le cas d'un répertoire nous aurions un `d` (directory)
        - les 3 symboles suivants `rw-` donnent les droits du propriétaire du fichier : lecture autorisée (r), écriture autorisée (w), exécution interdite (- à la place de x)
        - Les 3 symboles suivants `r--` donnent les droits du groupe lié au fichier : lecture autorisée (r), écriture interdite (- à la place de w), exécution interdite (- à laplace de x)
        - Les 3 symboles suivants `r--` donnent les droits des autres utilisateurs : lecture autorisée (r), écriture interdite (- à la place de w), exécution interdite (- à la place de x)

    - Le caractère suivant "1" donne le nombre de liens (nous n'étudierons pas cette notion ici)
    - Le premier `kelian` représente le nom du propriétaire du fichier
    - Le second `kelian` représente le nom du groupe lié au fichier
    - `0` représente la taille du fichier en octet (ici notre fichier est vide)
    - `Feb 20 14:59` donne la date et l'heure de la dernière modification du fichier 
    - `TODO.txt` est le nom du fichier

??? def "Définition des droits"
    ## Définition des droits {.hidden}

    ### Commande `chown`
    Cette commande permet de définir l'utilisateur et le groupe propriétaire du fichier :  
    `chown [options -R] utilisateur:groupe fichier`    
    L'option `-R` permet de modifier les droits de tous les fichiers et sous-dossiers contenus dans un répertoire.

    **Exemple :**

    ```shell 
    chown -R kelian:kelian /home/kelian/Documents/boulot/
    ```
    
    ### Commande `chmod`

    Les droits de lecture `w`, d'écriture `r` et d'exécution `x` sont modifiables avec la commande :  
    `chmod [options -R] droit fichier`
    
    Il existe 2 manière de définir les droits :

    #### En utilisant une forme littérale (exemple : rwx)
    
    ```shell
    chmod u+x,g=rw,o= /home/kelian/mon_script.py
    ```
    > - Ajoute (**+**) le droit d'exécution (**x**) à l'utilisateur propriétaire du fichier (**u**)
    > - Attribue (**=**) les droits de lecture et écriture au groupe propriétaire du fichier (**g**)
    > - Attribue aucun droit aux autres (others: **o** ).
    ```shell
    chmod a-x /home/kelian/mon_script.py
    ```
    > - Enlève (**-**) le droit d'exécution à tout le monde (all: **a**).
    
    #### En utilisant la forme octale (exemple : 754)

    Plus directement, on peut exprimer les droits en les regroupant en 3 groupes de 3 bits, soit sur **3 chiffres exprimés en base 8** (octale).

    <table>
    <tr>
        <td></td>
        <td colspan="3">Propriétaire : u</td>
        <td colspan="3">Groupe : g</td>
        <td colspan="3">Les autres : o</td>    
    </tr>
    <tr>
        <td>Droits</td>
        <td>r</td>
        <td>w</d>
        <td>-</td>
        <td>r</td>
        <td>-</td>
        <td>-</td>
        <td>r</td>
        <td>-</td>
        <td>-</td>
    </tr>
    <tr>
        <td>Binaire</td>
        <td>1</td>
        <td>1</d>
        <td>0</td>
        <td>1</td>
        <td>0</td>
        <td>0</td>
        <td>1</td>
        <td>0</td>
        <td>0</td>
    </tr>
    <tr>
        <td>Octale</td>
        <td colspan="3">6</td>
        <td colspan="3">4</td>
        <td colspan="3">4</td> 
    </tr>
    </table>

    Ainsi l'attribution des droits `rw-r--r--` se fera avec la commande :
    
    ```shell
    chmod 644 /home/kelian/mon_script.py
    ```

    !!! python "Remarque : les octales en python"
        On passe du binaire (base 2) à l'octale (base 8) et inversement de la manière suivante :

        ```pycon   
        >>> oct(int('110100100', 2))
        '0o644'
        >>> bin(int('644', 8))
        '0b110100100'
        ```

{{exo("Entraînement", 1)}}

    **1.** Quel est le rôle de la commande `chmod` ?
    ??? success "Réponse"
        Définir les droits d'accès à un fichier ou un répertoire.
    **2.** Qu'est-ce que la commande `chown` permet de faire ?
    ??? success "Réponse"
        Changer la propriété d'un fichier ou d'un répertoire.
    **3.** Quel est l'effet de la commande `chmod 755 fichier` ?
    ??? success "Réponse"
        Donne les droits de lecture, d'écriture et d'exécution au propriétaire, les droits de lecture et d'exécution au groupe, et les droits de lecture et d'exécution aux autres.
    **4.** Comment donner les droits d'exécution à un fichier pour tous les utilisateurs avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod a+x fichier`.
    **5.** Quel est l'effet de la commande `chown utilisateur:groupe fichier` ?
    ??? success "Réponse"
        Change la propriété du fichier pour l'utilisateur et le groupe spécifiés.
    **6.** Comment retirer le droit d'écriture à un fichier pour le groupe avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod g-w fichier`.
    **7.** Quel est l'effet de la commande `chmod 777 fichier` ?
    ??? success "Réponse"
        Donne tous les droits (lecture, écriture et exécution) au propriétaire, au groupe et aux autres.
    **8.** Comment changer la propriété de groupe d'un fichier avec `chown` ?
    ??? success "Réponse"
        Utiliser la commande `chown :groupe fichier`.
    **9.** Quel est l'effet de la commande `chmod u+x fichier` ?
    ??? success "Réponse"
        Donne le droit d'exécution au propriétaire du fichier.
    **10.** Comment définir les droits d'un fichier en lecture seule pour tous les utilisateurs avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod 444 fichier`.
    **11.** Quel est l'effet de la commande `chmod -R 644 répertoire` ?
    ??? success "Réponse"
        Définit récursivement les droits de lecture et d'écriture pour le propriétaire, et les droits de lecture pour le groupe et les autres, pour tous les fichiers et sous-répertoires du répertoire spécifié.
    **12.** Comment donner les droits de lecture et d'écriture à un fichier pour le propriétaire avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod u+rw fichier`.
    **13.** Quel est l'effet de la commande `chown utilisateur fichier` ?
    ??? success "Réponse"
        Change la propriété du fichier pour l'utilisateur spécifié.
    **14.** Comment retirer le droit d'exécution à un fichier pour le propriétaire avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod u-x fichier`.
    **15.** Quel est l'effet de la commande `chmod g+r fichier` ?
    ??? success "Réponse"
        Donne le droit de lecture au groupe du fichier.
    **16.** Comment définir les droits d'un fichier en écriture seule pour le propriétaire avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod u+w fichier`.
    **17.** Quel est l'effet de la commande `chmod o-r fichier` ?
    ??? success "Réponse"
        Retire le droit de lecture aux autres utilisateurs du fichier.
    **18.** Comment donner les droits de lecture et d'exécution à un fichier pour le groupe avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod g+rx fichier`.
    **19.** Quel est l'effet de la commande `chown :groupe fichier` ?
    ??? success "Réponse"
        Change la propriété de groupe du fichier pour le groupe spécifié.
    **20.** Comment définir les droits d'un fichier en lecture et écriture pour le propriétaire et en lecture seule pour les autres avec `chmod` ?
    ??? success "Réponse"
        Utiliser la commande `chmod 664 fichier`.