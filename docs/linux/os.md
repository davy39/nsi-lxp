---
title: Familles d'OS
icon: material/numeric-1-circle
---

# Familles de systèmes d'exploitation {:#os}

{{tube_sci("Un système d'exploitation c'est quoi?","6753ca2c-d07e-4b63-8d4e-629016e4ce1c")}}

??? histoire "Un peu d'histoire"
    {{tube_sci("Histoire des systèmes d'exploitation","29962c73-c069-4f5a-a2dd-a98148e9fa14")}}

??? def "De Multics à Linux :simple-linux:"
    ## De Multics à Linux {.hidden}

    Les tout premiers ordinateurs n'avaient pas vraiment de système d'exploitation (les logiciels devaient gérer la partie matérielle). Il faut attendre 1965 pour voir arriver le premier système d'exploitation multitâche (capable d'exécuter plusieurs programmes en "même temps") et multi-utilisateur : **Multics**.

    Le système d'exploitation **UNIX** voit le jour à la toute fin des années 60. Il a été conçu par Ken Thompson et Dennis Ritchie des laboratoires Bell. Le système d'exploitation Multics ne fonctionnait que sur des ordinateurs extrêmement chers, l'idée de Thompson et Ritchie était de concevoir un système d'exploitation pour les ordinateurs un peu moins onéreux (mais on ne pouvait tout de même pas parler à cette époque d'informatique "grand public", les ordinateurs étaient encore réservés aux centres de recherche et aux grandes entreprises). 

    Le système UNIX est un système dit "**propriétaire**" (certaines personnes disent "**privateur**"), c'est-à-dire un système non libre. Il est à la base de nombreux OS dont Apple MacOS, et à inspiré le système Linux.

    --8<-- 'docs/assets/images/unix.svg'

    En 1991, un étudiant finlandais, **Linus Torvalds**, décide de créer un clone libre d'UNIX en ne partant de rien (on dit "from scratch" en anglais) puisque le code source d'UNIX n'est pas public. Ce clone d'UNIX va s'appeler **Linux** (Linus+UNIX). Linux est aujourd'hui le système d'exploitation le plus utilisé au monde si on tient compte des serveurs et des smartphones (**Android** est dérivé d'un système Linux).

    {{tube_sci("How Linux is Built", "34867ef3-bf75-4c74-ace1-59b5c10cf0ee")}}

    ??? histoire "Un peu d'histoire"
        {{tube_sci("Histoire d'UNIX","f74ca448-7093-492e-9240-65e832042f2f")}}

!!! info "Libres ou propriétaires ?"
    D'après Wikipédia : "Un **logiciel libre** est un logiciel dont l'utilisation, l'étude, la modification et la duplication par autrui en vue de sa diffusion sont permises, techniquement et légalement, ceci afin de garantir certaines libertés induites, dont le contrôle du programme par l'utilisateur et la possibilité de partage entre individus". 

    Le système UNIX ne respecte pas ces droits (par exemple le **code source** d'UNIX n'est pas disponible, l'étude d'UNIX est donc impossible), UNIX est donc un système "propriétaire" (le contraire de "libre"). Attention qui dit logiciel libre ne veut pas forcement dire logiciel gratuit (même si c'est souvent le cas), la confusion entre "libre" et "gratuit" vient de l'anglais puisque **free** veut à la fois dire "libre", mais aussi "gratuit".

??? def "Microsoft Windows :material-microsoft-windows:"
    ## Microsoft Windows :material-microsoft-windows:  {.hidden}

    Microsoft a été créée par Bill Gates et Paul Allen en 1975. Microsoft est surtout connue pour son système d'exploitation **Windows**. Windows est un système d'exploitation "propriétaire", la première version de Windows date 1983, mais à cette date Windows n'est qu'un ajout sur un autre système d'exploitation nommé **MS-DOS**. Aujourd'hui Windows reste le système d'exploitation le plus utilisé au monde sur les ordinateurs grand public (ordinateurs personnels), il faut dire que l'achat de Windows est quasiment imposé lorsque l'on achète un ordinateur dans le commerce, car oui, quand vous achetez un ordinateur neuf, une partie de la somme que vous versez termine dans les poches de Microsoft. Il est possible de se faire rembourser la licence Windows, mais cette opération est relativement complexe.


??? def "Apple MacOS :material-apple:"
    ## Apple MacOS :material-apple: {.hidden}

    Enfin pour terminer, quelques mots sur le système d'exploitation des ordinateurs de marque Apple : tous les ordinateurs d'Apple sont livrés avec le système d'exploitation **macOS**. Ce système macOS est un système d'exploitation UNIX, c'est donc un système d'exploitation propriétaire.


