---
title: Encodage
icon: material/cpu-64-bit
---
# :material-cpu-64-bit: L'encodage des données :material-cpu-64-bit: {: #encodage }


!!! abstract "Contenu du chapitre"

    L'encodage des données est un concept fondamental en informatique qui consiste à représenter l'information sous forme de séquences de **bits**. Un bit est l'unité de base de l'information **binaire**, pouvant prendre la valeur `0` ou `1`. Les bits sont regroupés en **octets**, qui sont des ensembles de 8 bits. L'encodage des données permet de stocker, transmettre et traiter des informations de manière numérique, en utilisant des combinaisons de bits pour représenter des caractères, des nombres, des images, des vidéos, etc. 

    - Le chapitre sur les nombres entiers positifs abordera la représentation et l'encodage des [nombres entiers positifs](./entier-positif.md) en informatique, avec l'introduction de la notion mathématique de base de dénombrement.
    - Dans le chapitre sur les [nombres entiers, y compris les nombres négatifs (signés +/-)](./entier-negatif.md), nous explorerons la manière dont les nombres entiers sont encodés en tenant compte de leur signe. 
    - Le prochain chapitre portera sur les [nombres réels à virgule](./non-entier.md), mettant en évidence la difficulté à représenter les nombres non entiers en informatique.
    - Enfin, le chapitre sur les [caractères](./caractere.md) traitera de la représentation des caractères et des symboles dans un système informatique, en particulier les normes ASCII et UTF-8. 

{{tube_sci("Bit et octets, unité de quantité d'information" , '24071e74-0b86-44f6-8d42-2f792160ef17')}}
