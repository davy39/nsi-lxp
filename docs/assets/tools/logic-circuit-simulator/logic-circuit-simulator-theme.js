// Adaptation de Logic Simulator au thème -> plus besoin
$(window).on('load', function() {
	var body = document.body;
    if (body.getAttribute('data-md-color-scheme') === 'slate') {
      Logic.setDarkMode('#000');
    } else if (body.getAttribute('data-md-color-scheme') === 'default') {
      Logic.setDarkMode(false);
    }
    var darkInput = document.querySelector('input[data-md-color-media="(prefers-color-scheme: dark)"]');
    var lightInput = document.querySelector('input[data-md-color-media="(prefers-color-scheme: light)"]');
    darkInput.addEventListener('change', function() {
      Logic.setDarkMode('#000');
    });
    lightInput.addEventListener('change', function() {
      Logic.setDarkMode(false);
    });
});

