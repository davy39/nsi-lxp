---
title: Introduction
icon: material/information
hide:
    - toc
    - navigation
--- 
# :material-information: Introduction :material-information:

!!! warning "Site en construction"
    Ce site internet constitue le cours de **spécialité Numérique et Sciences de l'Informatique (NSI)** du Lycée Expérimental de Saint-Nazaire.
    
    Il est encore en construction, et sera probablement en permanente évolution. N'hésitez à en critiquer le contenu et à me proposer des pistes d'amélioration dans le [fil de discussion](https://github.com/Lycee-Experimental/nsi-lxp/discussions/1).

!!! question "Par où commencer ?"
    - Un début de parcours peut consister à se pencher sur les [fondements de l'informatique](--fondements) puis l'[encodage des données](--encodage), afin de se faire une idée générale sur le fonctionnement des ordinateurs.
    - Le monde des [systèmes d'exploitation et de linux](--linux) peut être traité indépendamment.
    - E parallèle, il est conseillé de se mettre dès que possible à l'apprentissage de python : un bon moyen est de suivre un parcours sur [FutureCoder](https://fr.futurecoder.io).
    - Des [quiz](--quizz) sont disponibles pour tester vos connaissances sur les différents chapitres.
    - Enfin, les sujets bac de la banque (1ère) et annales (Term) sont accessibles [ici](--sujets-bac).

??? info "A propos de ce site"
    - Ce site a été conçu en mobilisant de nombreux [outils web](--outils), un grand merci à tou·te·s leurs contributeur·ices !
    - Je me suis grandement inspiré de sites de collègues, un grand merci à ell·eux de partager leur travail ! Qu'iels se sentent libres d'utiliser le mien comme iels le souhaitent, dès lors qu'il n'en est pas fait d'utilisation commerciale. Une liste de sites est accessible [ici](--ressources).
