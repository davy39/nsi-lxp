---
title: Algorithmes
icon: material/robot
---
# :material-robot: Introduction à l'algorithmique :material-robot: {: #algo }
  
    
!!! def "Définition d'un algorithme"

    <figure style="width:20%; float:right; margin: 10px;">
    ![Al Kwârizmî](/assets/images/al_khwarizmi.jpg)
    <figcaption>Mohammed Al Kwârizmî, c. 780-850 ap JC </figcaption>
    </figure>

    Les algorithmes sont bien plus anciens que l'informatique. L'étymologie vient de **Al Kwârizmî**, un savant persan du 8ème Siècle. Né dans les années 780 dans l'actuel Ouzbékistan, mort vers 850 à Bagdad - actuel Irak, il écrivit un livre important sur la résolution des équations (le mot algèbre vient du titre de ce livre).

    On peut définir un algorithme comme **une suite finie d'instructions permettant de résoudre un problème**, c’est-à-dire qui permet, à partir de **données** de départ, d'obtenir un **résultat** recherché.

    Ainsi une simple recette de cuisine est un algorithme.

    Un algorithme peut être traduit en **programme** que peut interpréter un ordinateur : on parle d’**implémentation** d'un algorithme.

