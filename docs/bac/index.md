---
title: Bac
icon: material/certificate
---

# :material-certificate: Le baccalauréat :material-certificate:

Vous trouverez dans cette section les informations concernant la Spécialité NSI au baccalauréat. 

- Le programme est distillé peu à peu dans chaque partie de ce site.

- Les [sujets](--sujets-bac) de la banque (1ère) et annales (Term)

- Des [ressources](--ressources) pour accompagner vos apprentissages.