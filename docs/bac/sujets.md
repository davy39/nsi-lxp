---
title: Sujets Bac
icon: material/file-question
---
# Les sujets de la banque pour les premières, et les annales pour les Term ! {: #sujets-bac }

??? abstract "Première"
    Tous les sujets **QCM** de la banque officielle sont ici :

    [1](++sujets/1re/sujet1_pdf) - 
    [2](++sujets/1re/sujet2_pdf) - 
    [3](++sujets/1re/sujet3_pdf) - 
    [4](++sujets/1re/sujet4_pdf) - 
    [5](++sujets/1re/sujet5_pdf) - 
    [6](++sujets/1re/sujet6_pdf) - 
    [7](++sujets/1re/sujet7_pdf) - 
    [8](++sujets/1re/sujet8_pdf) - 
    [9](++sujets/1re/sujet9_pdf) - 
    [10](++sujets/1re/sujet10_pdf) - 
    [12](++sujets/1re/sujet12_pdf) - 
    [13](++sujets/1re/sujet13_pdf) - 
    [14](++sujets/1re/sujet14_pdf) - 
    [15](++sujets/1re/sujet15_pdf) - 
    [16](++sujets/1re/sujet16_pdf) - 
    [17](++sujets/1re/sujet17_pdf) - 
    [18](++sujets/1re/sujet18_pdf) - 
    [19](++sujets/1re/sujet19_pdf) - 
    [20](++sujets/1re/sujet20_pdf) - 
    [21](++sujets/1re/sujet21_pdf) - 
    [22](++sujets/1re/sujet22_pdf) - 
    [24](++sujets/1re/sujet24_pdf) - 
    [25](++sujets/1re/sujet25_pdf) - 
    [26](++sujets/1re/sujet26_pdf) - 
    [27](++sujets/1re/sujet27_pdf) - 
    [28](++sujets/1re/sujet28_pdf) - 
    [29](++sujets/1re/sujet29_pdf) - 
    [30](++sujets/1re/sujet30_pdf) - 
    [31](++sujets/1re/sujet31_pdf) - 
    [32](++sujets/1re/sujet32_pdf) - 
    [33](++sujets/1re/sujet33_pdf) - 
    [34](++sujets/1re/sujet34_pdf) - 
    [35](++sujets/1re/sujet35_pdf) - 
    [36](++sujets/1re/sujet36_pdf) - 
    [37](++sujets/1re/sujet37_pdf) - 
    [38](++sujets/1re/sujet38_pdf) - 
    [39](++sujets/1re/sujet39_pdf) - 
    [40](++sujets/1re/sujet40_pdf) - 
    [41](++sujets/1re/sujet41_pdf) - 
    [42](++sujets/1re/sujet42_pdf) - 
    [43](++sujets/1re/sujet43_pdf) - 
    [44](++sujets/1re/sujet44_pdf) - 
    [45](++sujets/1re/sujet45_pdf) - 
    [46](++sujets/1re/sujet46_pdf) - 
    [47](++sujets/1re/sujet47_pdf) - 
    [48](++sujets/1re/sujet48_pdf) - 
    [49](++sujets/1re/sujet49_pdf) - 
    [50](++sujets/1re/sujet50_pdf) - 
    [51](++sujets/1re/sujet51_pdf) - 
    [52](++sujets/1re/sujet52_pdf) - 
    [53](++sujets/1re/sujet53_pdf) - 
    [54](++sujets/1re/sujet54_pdf) - 
    [55](++sujets/1re/sujet55_pdf) - 
    [56](++sujets/1re/sujet56_pdf) - 
    [57](++sujets/1re/sujet57_pdf) - 
    [58](++sujets/1re/sujet58_pdf) - 
    [59](++sujets/1re/sujet59_pdf) - 
    [60](++sujets/1re/sujet60_pdf) - 
    [61](++sujets/1re/sujet61_pdf) - 
    [62](++sujets/1re/sujet62_pdf) - 
    [63](++sujets/1re/sujet63_pdf) - 
    [64](++sujets/1re/sujet64_pdf) - 
    [65](++sujets/1re/sujet65_pdf) - 
    [66](++sujets/1re/sujet66_pdf) - 
    [67](++sujets/1re/sujet67_pdf) - 
    [68](++sujets/1re/sujet68_pdf) - 
    [69](++sujets/1re/sujet69_pdf) - 
    [70](++sujets/1re/sujet70_pdf) - 
    [71](++sujets/1re/sujet71_pdf) - 
    [72](++sujets/1re/sujet72_pdf) - 
    [73](++sujets/1re/sujet73_pdf) - 
    [74](++sujets/1re/sujet74_pdf) - 
    [75](++sujets/1re/sujet75_pdf) - 
    [76](++sujets/1re/sujet76_pdf) - 
    [77](++sujets/1re/sujet77_pdf) - 
    [78](++sujets/1re/sujet78_pdf) - 
    [79](++sujets/1re/sujet79_pdf) - 
    [80](++sujets/1re/sujet80_pdf) - 
    [81](++sujets/1re/sujet81_pdf) - 
    [82](++sujets/1re/sujet82_pdf) - 
    [83](++sujets/1re/sujet83_pdf) - 
    [84](++sujets/1re/sujet84_pdf) - 
    [85](++sujets/1re/sujet85_pdf) - 
    [86](++sujets/1re/sujet86_pdf) - 
    [87](++sujets/1re/sujet87_pdf) - 
    [88](++sujets/1re/sujet88_pdf) - 
    [89](++sujets/1re/sujet89_pdf) - 
    [90](++sujets/1re/sujet90_pdf) - 
    [91](++sujets/1re/sujet91_pdf) - 
    [92](++sujets/1re/sujet92_pdf) - 
    [93](++sujets/1re/sujet93_pdf) - 
    [94](++sujets/1re/sujet94_pdf) - 
    [95](++sujets/1re/sujet95_pdf) - 
    [96](++sujets/1re/sujet96_pdf) - 
    [97](++sujets/1re/sujet97_pdf) - 
    [98](++sujets/1re/sujet98_pdf) - 
    [99](++sujets/1re/sujet99_pdf) - 
    [100](++sujets/1re/sujet100_pdf) - 
    [101](++sujets/1re/sujet101_pdf) - 
    [102](++sujets/1re/sujet102_pdf) - 
    [103](++sujets/1re/sujet103_pdf) - 
    [104](++sujets/1re/sujet104_pdf) - 
    [105](++sujets/1re/sujet105_pdf) - 
    [106](++sujets/1re/sujet106_pdf) - 
    [107](++sujets/1re/sujet107_pdf) - 
    [108](++sujets/1re/sujet108_pdf) - 
    [109](++sujets/1re/sujet109_pdf) - 
    [110](++sujets/1re/sujet110_pdf) - 
    [111](++sujets/1re/sujet111_pdf) - 
    [112](++sujets/1re/sujet112_pdf) - 
    [113](++sujets/1re/sujet113_pdf) - 
    [114](++sujets/1re/sujet114_pdf) - 
    [115](++sujets/1re/sujet115_pdf) - 
    [116](++sujets/1re/sujet116_pdf) - 
    [117](++sujets/1re/sujet117_pdf) - 
    [118](++sujets/1re/sujet118_pdf) - 
    [119](++sujets/1re/sujet119_pdf) - 
    [120](++sujets/1re/sujet120_pdf) - 
    [121](++sujets/1re/sujet121_pdf) - 
    [122](++sujets/1re/sujet122_pdf) - 
    [123](++sujets/1re/sujet123_pdf) - 
    [124](++sujets/1re/sujet124_pdf) - 
    [125](++sujets/1re/sujet125_pdf) - 
    [126](++sujets/1re/sujet126_pdf) - 
    [127](++sujets/1re/sujet127_pdf) - 
    [128](++sujets/1re/sujet128_pdf) - 
    [129](++sujets/1re/sujet129_pdf) - 
    [130](++sujets/1re/sujet130_pdf) - 
    [131](++sujets/1re/sujet131_pdf) - 
    [132](++sujets/1re/sujet132_pdf) - 
    [133](++sujets/1re/sujet133_pdf) - 
    [134](++sujets/1re/sujet134_pdf) - 
    [137](++sujets/1re/sujet137_pdf) - 
    [138](++sujets/1re/sujet138_pdf) - 
    [139](++sujets/1re/sujet139_pdf) - 
    [140](++sujets/1re/sujet140_pdf) - 
    [141](++sujets/1re/sujet141_pdf) - 
    [142](++sujets/1re/sujet142_pdf) - 
    [143](++sujets/1re/sujet143_pdf) - 
    [144](++sujets/1re/sujet144_pdf) - 
    [145](++sujets/1re/sujet145_pdf) - 
    [146](++sujets/1re/sujet146_pdf) - 
    [147](++sujets/1re/sujet147_pdf) - 
    [148](++sujets/1re/sujet148_pdf) - 
    [149](++sujets/1re/sujet149_pdf) - 
    [150](++sujets/1re/sujet150_pdf) - 
    [151](++sujets/1re/sujet151_pdf) - 
    [152](++sujets/1re/sujet152_pdf) - 
    [153](++sujets/1re/sujet153_pdf) - 
    [154](++sujets/1re/sujet154_pdf) - 
    [156](++sujets/1re/sujet156_pdf) - 
    [157](++sujets/1re/sujet157_pdf) - 
    [158](++sujets/1re/sujet158_pdf) - 
    [159](++sujets/1re/sujet159_pdf) - 
    [160](++sujets/1re/sujet160_pdf) - 
    [161](++sujets/1re/sujet161_pdf) - 
    [162](++sujets/1re/sujet162_pdf) - 
    [163](++sujets/1re/sujet163_pdf) - 
    [164](++sujets/1re/sujet164_pdf) - 
    [165](++sujets/1re/sujet165_pdf) - 
    [166](++sujets/1re/sujet166_pdf) - 
    [167](++sujets/1re/sujet167_pdf) - 
    [168](++sujets/1re/sujet168_pdf) - 
    [169](++sujets/1re/sujet169_pdf) - 
    [170](++sujets/1re/sujet170_pdf) - 
    [171](++sujets/1re/sujet171_pdf) - 
    [172](++sujets/1re/sujet172_pdf) - 
    [173](++sujets/1re/sujet173_pdf) - 
    [174](++sujets/1re/sujet174_pdf) - 
    [175](++sujets/1re/sujet175_pdf) - 
    [176](++sujets/1re/sujet176_pdf) - 
    [177](++sujets/1re/sujet177_pdf) - 
    [178](++sujets/1re/sujet178_pdf) - 
    [179](++sujets/1re/sujet179_pdf) - 
    [180](++sujets/1re/sujet180_pdf) - 
    [181](++sujets/1re/sujet181_pdf) - 
    [182](++sujets/1re/sujet182_pdf) - 
    [183](++sujets/1re/sujet183_pdf) - 
    [184](++sujets/1re/sujet184_pdf) - 
    [185](++sujets/1re/sujet185_pdf) - 
    [187](++sujets/1re/sujet187_pdf) - 
    [188](++sujets/1re/sujet188_pdf) - 
    [189](++sujets/1re/sujet189_pdf) - 
    [190](++sujets/1re/sujet190_pdf) - 
    [191](++sujets/1re/sujet191_pdf) - 
    [192](++sujets/1re/sujet192_pdf) - 
    [193](++sujets/1re/sujet193_pdf) - 
    [194](++sujets/1re/sujet194_pdf) - 
    [195](++sujets/1re/sujet195_pdf) - 
    [196](++sujets/1re/sujet196_pdf) - 
    [197](++sujets/1re/sujet197_pdf) - 
    [198](++sujets/1re/sujet198_pdf) - 
    [199](++sujets/1re/sujet199_pdf) - 
    [200](++sujets/1re/sujet200_pdf) - 
    [201](++sujets/1re/sujet201_pdf) - 
    [202](++sujets/1re/sujet202_pdf) - 
    [203](++sujets/1re/sujet203_pdf) - 
    [204](++sujets/1re/sujet204_pdf) - 
    [205](++sujets/1re/sujet205_pdf) - 
    [206](++sujets/1re/sujet206_pdf) - 
    [207](++sujets/1re/sujet207_pdf) - 
    [208](++sujets/1re/sujet208_pdf) - 
    [209](++sujets/1re/sujet209_pdf) - 
    [210](++sujets/1re/sujet210_pdf) - 
    [211](++sujets/1re/sujet211_pdf) - 
    [212](++sujets/1re/sujet212_pdf) - 
    [213](++sujets/1re/sujet213_pdf) - 
    [214](++sujets/1re/sujet214_pdf) - 
    [215](++sujets/1re/sujet215_pdf) - 
    [216](++sujets/1re/sujet216_pdf) - 
    [217](++sujets/1re/sujet217_pdf) - 
    [218](++sujets/1re/sujet218_pdf) - 
    [219](++sujets/1re/sujet219_pdf) - 
    [220](++sujets/1re/sujet220_pdf) - 
    [221](++sujets/1re/sujet221_pdf) - 
    [222](++sujets/1re/sujet222_pdf) - 
    [223](++sujets/1re/sujet223_pdf) - 
    [224](++sujets/1re/sujet224_pdf) - 
    [225](++sujets/1re/sujet225_pdf) - 
    [226](++sujets/1re/sujet226_pdf) - 
    [227](++sujets/1re/sujet227_pdf) - 
    [228](++sujets/1re/sujet228_pdf) - 
    [230](++sujets/1re/sujet230_pdf) - 
    [231](++sujets/1re/sujet231_pdf) - 
    [232](++sujets/1re/sujet232_pdf) - 
    [233](++sujets/1re/sujet233_pdf) - 
    [234](++sujets/1re/sujet234_pdf) - 
    [235](++sujets/1re/sujet235_pdf) - 
    [236](++sujets/1re/sujet236_pdf) - 
    [237](++sujets/1re/sujet237_pdf) - 
    [238](++sujets/1re/sujet238_pdf) - 
    [239](++sujets/1re/sujet239_pdf) - 
    [240](++sujets/1re/sujet240_pdf) - 
    [241](++sujets/1re/sujet241_pdf) - 
    [242](++sujets/1re/sujet242_pdf) - 
    [243](++sujets/1re/sujet243_pdf) - 
    [244](++sujets/1re/sujet244_pdf) - 
    [245](++sujets/1re/sujet245_pdf) - 
    [246](++sujets/1re/sujet246_pdf) - 
    [247](++sujets/1re/sujet247_pdf) - 
    [249](++sujets/1re/sujet249_pdf) - 
    [250](++sujets/1re/sujet250_pdf) - 
    [251](++sujets/1re/sujet251_pdf) - 
    [252](++sujets/1re/sujet252_pdf) - 
    [253](++sujets/1re/sujet253_pdf) - 
    [254](++sujets/1re/sujet254_pdf) - 
    [255](++sujets/1re/sujet255_pdf) - 
    [256](++sujets/1re/sujet256_pdf) - 
    [257](++sujets/1re/sujet257_pdf) - 
    [258](++sujets/1re/sujet258_pdf) - 
    [259](++sujets/1re/sujet259_pdf) - 
    [260](++sujets/1re/sujet260_pdf) - 
    [261](++sujets/1re/sujet261_pdf) - 
    [262](++sujets/1re/sujet262_pdf) - 
    [263](++sujets/1re/sujet263_pdf) - 
    [264](++sujets/1re/sujet264_pdf) - 
    [265](++sujets/1re/sujet265_pdf) - 
    [266](++sujets/1re/sujet266_pdf) - 
    [267](++sujets/1re/sujet267_pdf) - 
    [268](++sujets/1re/sujet268_pdf) - 
    [269](++sujets/1re/sujet269_pdf)


??? abstract "Terminale"
    === "Épreuves écrites (3h30)"

        !!! warning "A noter"
            A partir de 2023, vous ne pouvez plus choisir 3 des 5 exercices comme auparavant : les 3 exercices sont maintenant imposés.

        | Sujets | Corrigé | Contenus | 
        | :------------: | :-------------: | :------------ |
        [2024 - Sujet Zéro-A](++sujets/term/24-NSIZERO-A_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIZERO-A-corrige_pdf) | Ex 1 [6 points] : (Réseau et architecture 1re ) (Protocoles Tle) Architecture matérielle, réseaux, routeurs et protocoles de routage.<br> Ex 2 [6 points] : (Algorithmique, Dictionnaires, Listes 1re) Listes, dictionnaires et programmation de base en Python.<br>Ex 3 [8 points] : (Arbres, Graphes, BDD, SQL Tle) Graphes, algorithmes sur les graphes, bases de données et requêtes SQL.
        [2024 - Sujet Zéro-B](++sujets/term/24-NSIZERO-B_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIZERO-B-corrige_pdf) | Ex 1 [6 points] : (Récursivité, listes, prog Dynamique Tle) Notion de listes, récursivité et programmation dynamique<br>Ex 2 [6 points] : (UNIX, SE, Piles et Files Tle) Systèmes d’exploitation, commandes UNIX, structures de données (de type LIFO et FIFO) et processus<br>Ex 3 [8 points] : (Dictionnaires, POO, BDD et SQL Tle) Programmation Python (dictionnaire), Programmation orientée objet, bases de données relationnelles et requêtes SQL.
        [2024 - Etranger - jour 1](++sujets/term/24-NSIJ1G11_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ1G11-corrige_pdf) | Ex 1 [6 points] : Programmation dynamique, Graphes, réseaux<br>Ex 2 [6 points] : Réseaux (1re) et protocoles de routage<br>Ex 3 [8 points] : POO, Bases de données et SQL
        [2024 - Etranger - jour 2](++sujets/term/24-NSIJ2G11_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ2G11-corrige_pdf) | Ex 1 [6 points] : Python en général, programmation orientée objet et récursivité.<br>Ex 2 [6 points] : programmation objet, récursivité, arbres binaires et systèmes d’exploitation (1re).<br>Ex 3 [8 points] : codage binaire (1re), bases de données relationnelles et requêtes SQL.
        [2024 - Amérique Nord - Sujet 1](++sujets/term/24-NSIJ1AN1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ1AN1-corrige_pdf) | Ex 1 [6 points] : POO, File, Ordonnancement et Interblocage<br>Ex 2 [6 points] : Graphes<br>Ex 3 [8 points] : Python, Bases de Données, SQL
        [2024 - Amérique Nord - Sujet 2](++sujets/term/24-NSIJ2AN1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ2AN1-corrige_pdf) | Ex 1 [6 points] : Algo de Tri, Programmation, récursivité (Erreur dans l'exercice avec beaucoup d'imprécisions)<br>Ex 2 [6 points] : SQL, BDD<brEx 3 [8 points] : POO, Structures de données, réseaux et architecture
        [2024 - Asie Sujet 1](++sujets/term/24-NSIJ1JA1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ1JA1-corrige_pdf) | Ex 1 [6 points] : la programmation Python, la programmation orientée objet et l’algorithmique<br>Ex 2 [6 points] :  graphes, programmation, structure de pile et algorithmique des graphes.<br>Ex 3 [8 points] : programmation Python, programmation orientée objet, bases de données relationnelles et requêtes SQL.
        [2024 - Asie Sujet 2](++sujets/term/24-NSIJ2JA1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ2JA1-corrige_pdf) | Ex 1 [6 points] : la programmation Python (listes, dictionnaires 1re) et la méthode “diviser pour régner”.<br>Ex 2 [6 points] : programmation orientée objet et structures de données linéaires.<br>Ex 3 [8 points] : bases de données relationnelles, requêtes SQL et programmation en Python.
        [2024 - Métropole Sujet 1](++sujets/term/24-NSIJ1ME1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ1ME1-corrige_pdf) | Ex 1 [6 points] : la programmation objet en Python et les graphes.<br>Ex 2 [6 points] : protocoles de routage, de sécurité des communications et de base de données relationnelle.<br>Ex 3 [8 points] : programmation orientée objet sur les arbres binaires de recherche et récursivité.
        [2024 - Métropole Sujet 2](++sujets/term/24-NSIJ2ME1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ2ME1-corrige_pdf) | Ex 1 [6 points] : bases de données relationnelles, langage SQL et protocoles de sécurisation.<br>Ex 2 [6 points] : programmation orientée objets, tris, algorithmes gloutons, récursivité et assertions<br>Ex 3 [8 points] : programmation orientée objet, graphes et utilise la structure de données dictionnaire.
        [2024 - Polynésie Sujet 1](++sujets/term/24-NSIJ1PO1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ1PO1-corrige_pdf) | Ex 1 [6 points] : graphes et protocoles réseau.<br>Ex 2 [6 points] : récursivité.<br>Ex 3 [8 points] : programmation Python (dictionnaire), bases de données relationnelles et requêtes SQL.
        [2024 - Polynésie Sujet 2](++sujets/term/24-NSIJ2PO1_pdf)|[:material-alpha-c-circle:](++sujets/term/24-NSIJ2PO1-corrige_pdf) | Ex 1 [6 points] :  structures de données FILE et PILE, graphes et algorithmes de parcours.<br>Ex 2 [6 points] : arbres binaires de recherche, POO et récursivité<br>Ex 3 [8 points] : protocoles réseau, bases de données relationnelles et requêtes SQL, algorithmique et programmation en Python.
        [2024 - Suède](++sujets/term/24-NSIG11BIS_pdf)| | Ex 1 [6 points] : listes de listes, programmation orientée objet. <br> Ex 2 [6 points] : architecture réseaux, routage. <br> Ex 3 [8 points] : programmation Python, traitement de données en table et bases de données relationnelles.
        [2023 - Etranger - jour 1](++sujets/term/23-NSIJ1G11_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ1G11-corrige_pdf) | Ex 1 [3 points] : Bases de données relationnelles et le langage SQL<br>Ex 2 [3 points] : Réseaux et protocoles de routage<br>Ex 3 [6 points] : Structures de données files
        [2023 - Etranger - jour 2](++sujets/term/23-NSIJ2G11_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ2G11-corrige_pdf) | Ex 1 [4 points] : Adressage IP et protocoles de routage<br>Ex 2 [4 points] : Bases de données relationnelles et langage SQL<br>Ex 3 [4 points] : programmation en Python, manipulation des chaines de caractères, arbres binaires de recherche et parcours de liste
        [2023 - Liban - jour 1](++sujets/term/23-NSIJ1LI1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ1LI1-corrige_pdf) | Ex 1 [3 points] : Algorithmique et programmation<br>Ex 2 [5 points] : Bases de données, représentation des données et réseaux<br>Ex 3 [4 points] : Piles, arbres et algorithmique
        [2023 - Réunion - jour 1](++sujets/term/23-NSIJ1LR1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ1LR1-corrige_pdf) | Ex 1 [4 points] : Réseaux et protocoles de routage<br>Ex 2 [4 points] : Programmation orientée objet et dictionnaires<br>Ex 3 [4 points] : Bases de données relationnelles et langage SQL
        [2023 - Métropole - jour 1](++sujets/term/23-NSIJ1ME1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ1ME1-corrige_pdf) | Ex 1 [3 points] : Base de données relationnelles et le langage SQL<br>Ex 2 [3 points] : Réseaux<br>Ex 3 [6 points] : Programmation orientée objet en Python et algorithmique
        [2023 - Polynésie - jour 1](++sujets/term/23-NSIJ1PO1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ1PO1-corrige_pdf) | Ex 1 [4 points] : Bases de données relationnelles et le langage SQL<br>Ex 2 [3 points] : * Gestions des processus et programmation orientée objet*<br>Ex 3 [5 points] : Algorithmique, programmation orientée objet et méthode diviser pour régner
        [2023 - Liban - jour 2](++sujets/term/23-NSIJ2LI1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ2LI1-corrige_pdf) | Ex 1 [4 points] : Bases de données<br>Ex 2 [3 points] : Réseaux, programmation Python et algorithmique<br>Ex 3 [5 points] : Arbres et algorithmique
        [2023 - Réunion - jour 2](++sujets/term/23-NSIJ2LR1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ2LR1-corrige_pdf) | Ex 1 [4 points] : Arbres binaires de recherche, programmation orientée objet et récursivité<br>Ex 2 [4 points] : Réseaux et protocoles de routage<br>Ex 3 [4 points] : Bases de données relationnelles et langage SQL
        [2023 - Métropole - jour 2](++sujets/term/23-NSIJ2ME1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ2ME1-corrige_pdf) | Ex 1 [3 points] : Protocoles Réseaux<br>Ex 2 [3 points] : Bases de données et langage SQL<br>Ex 3 [6 points] : Arbres binaires, files et programmation orientée objet
        [2023 - Polynésie - jour 2](++sujets/term/23-NSIJ2PO1_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIJ2PO1-corrige_pdf) | Ex 1 [4 points] : Arbres binaires de recherche, programmation orientée objet et récursivité<br>Ex 2 [4 points] : Bases de données, modèle relationnel et langage SQL<br>Ex 3 [4 points] : Architecture matérielle, processus et ordonnancement
        [2023 - Sujet Zéro-A](++sujets/term/23-NSIZERO-A_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIZERO-A-corrige_pdf) | Ex 1 [3 points] : bases de données et types construits de données<br>Ex 2 [3 points] : architecture matérielle, réseaux et systèmes d'exploitation<br>Ex 3 [6 points] : arbres binaires de recherche et programmation objet
        [2023 - Sujet Zéro-B](++sujets/term/23-NSIZERO-B_pdf)|[:material-alpha-c-circle:](++sujets/term/23-NSIZERO-B-corrige_pdf) | Ex 1 [4 points] : ligne de commande sous Linux, traitement de données en tables et bases de données<br>Ex 2 [4 points] : analyse et écriture de programmes récursifs<br>Ex 3 [4 points] : arbres binaires de recherche et notion d'objet
        [2022 - Amérique du Nord - Jour 1](++sujets/term/Amerique_Nord_mai_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Amerique_Nord_mai_2022_sujet_1_corrigé_pdf) | Ex 1 : bases de données (Vacances autrement) <br> Ex 2 : réseaux, protocoles de routage <br> Ex 3 : arbres binaires de recherche, algorithmique <br> Ex 4 : chaîne de caractère, tableau, Python <br> Ex 5 : files, tableaux, algorithmique
        [2022 - Amérique du Nord - Jour 2](++sujets/term/Amerique_Nord_mai_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Amerique_Nord_mai_2022_sujet_2_corrigé_pdf)| 	Ex 1 : liste, arbres binaires de recherche, POO<br>Ex 2 : systèmes d'exploitation, processus<br>Ex 3 : bases de données (site web salle de cinéma)<br>Ex 4 : arbres binaires, algorithmique<br>Ex 5 : tableaux à deux dimensions, Python 
        [2022 - Amérique du Sud - Jour 1](++sujets/term/Amerique_Sud_septembre_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Amerique_Sud_septembre_2022_sujet_1_corrigé_pdf)|Ex 1 : bases de données (maternité)<br>Ex 2 : programmation, algorithmes de tri<br>Ex 3 : arbres binaires<br>Ex 4 : systèmes d'exploitation, processus<br>Ex 5 : réseaux, protocoles de routage 
        [2022 - Amérique du Sud Jour 2](++sujets/term/Amerique_Sud_septembre_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Amerique_Sud_septembre_2022_sujet_2_corrigé_pdf) |  	Ex 1 : programmation, algorithmique, complexité<br>Ex 2 : réseaux, protocoles de routage<br>Ex 3 : bases de données (vente vélos électriques)<br>Ex 4 : Python, récursivité, diviser pour régner<br>Ex 5 : arbres binaires, POO, récursivité 
        [2022 - Asie - Jour 1](++sujets/term/Asie_mai_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Asie_mai_2022_sujet_1_corrigé_pdf)|Ex 1 : algorithmique, chaînes de caractères, complexité<br>Ex 2 : bases de données (restaurant site de réservation)<br>Ex 3 : systèmes d'exploitation, Linux<br>Ex 4 : POO, Python<br>Ex 5 : Python 
        [2022 - Asie - Jour 2](++sujets/term/Asie_mai_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Asie_mai_2022_sujet_2_corrigé_pdf)|Ex 1 : système d'exploitation Linux<br>Ex 2 : arbres binaires de recherche<br>Ex 3 : structures de donnée, programmation (jeu de la vie)<br>Ex 4 : bases de données (club de tennis)<br>Ex 5 : exécution de programmes, recherche et correction de bugs 
        [2022 - Centres étrangers - Jour 1](++sujets/term/CentresEtrangers_mai_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/CentresEtrangers_mai_2022_sujet_1_corrigé_pdf)|Ex 1 : structures de données, listes, p-uplets, dictionnaires<br>Ex 2 : structures de données, files, POO, Python<br>Ex 3 : structures de données, dictionnaires<br>Ex 4 : bases de données (centre météorologique)<br>Ex 5 : architectures matérielles, réseaux, protocoles de routage 
        [2022 - Centres étrangers - Jour 2](++sujets/term/CentresEtrangers_mai_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/CentresEtrangers_mai_2022_sujet_2_corrigé_pdf)| 	Ex 1 : Python, récursivité<br>Ex 2 : structures de données, dictionnaires, Python<br>Ex 3 : bases de données (enseignants, évaluations)<br>Ex 4 : structures de données, POO, Python<br>Ex 5 : architectures matérielles, systèmes d'exploitation, réseaux
        [2022 - Mayotte - Jour 1](++sujets/term/Mayotte_mai_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Mayotte_mai_2022_sujet_1_corrigé_pdf)|Ex 1 : structures de données, listes, piles, files<br>Ex 2 : structures de données, POO, Python<br>Ex 3 : bases de données (QCM NSI)<br>Ex 4 : algorithmique, arbres binaires<br>Ex 5 : réseau, protocole de routage 
        [2022 - Mayotte - Jour 2](++sujets/term/Mayotte_mai_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Mayotte_mai_2022_sujet_2_corrigé_pdf)| 	Ex 1 : structures de données, piles<br>Ex 2 : bases de données (hôtel)<br>Ex 3 : binaire, systèmes d'exploitation<br>Ex 4 : arbres binaires de recherche<br>Ex 5 : algorithmes, Python (TAKAZU) 
        [2022 - Polynésie - Jour 2](++sujets/term/Polynesie_mai_2022_pdf)|[:material-alpha-c-circle:](++sujets/term/Polynesie_mai_2022_corrigé_pdf)|Ex 1 : programmation, récursivité, Python<br>Ex 2 : architecture matérielle, ordonnancement, booléens<br>Ex 3 : bases de données (site web)<br>Ex 4 : structures de données, piles<br>Ex 5 : algorithmique, arbres binaires 
        [2022 - Métropole septembre](++sujets/term/Métropole_septembre_2022_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_septembre_2022_corrigé_pdf)|Ex 1 : protocoles de communication, réseaux, protocoles de routage<br>Ex 2 : tableaux, dictionnaires, Python<br>Ex 3 : bases de données (tableau périodique)<br>Ex 4 : piles, POO, Python<br>Ex 5 : traitement des données en tables, Python 
        [2022 - Métropole - Jour 1](++sujets/term/Métropole_mai_2022_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_mai_2022_sujet_1_corrigé_pdf)|Ex 1 : structures de données<br>Ex 2 : bases de données (cinéma)<br>Ex 3 : binaire, routage<br>Ex 4 : arbres binaires, diviser pour régner, récursivité<br>Ex 5 : POO 
        [2022 - Métropole - Jour 2](++sujets/term/Métropole_mai_2022_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_mai_2022_sujet_2_corrigé_pdf)|Ex 1 : arbres binaires de recherche, POO, récursivité<br>Ex 2 : structures de données (piles)<br>Ex 3 : réseaux, routage<br>Ex 4 : bases de données (musique)<br>Ex 5 : POO, diviser pour régner 
        [2021 - Centres étrangers - Jour 1](++sujets/term/CentresEtrangers_juin_2021_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/CentresEtrangers_juin_2021_sujet_1_corrigé_pdf)|Ex 1 : POO (codage César)<br>Ex 2 : dictionnaires, Python (vélos en location)<br>Ex 3 : arbres binaires de recherche<br>Ex 4 : réseaux, masque de sous-réseau, conversion décimal/binaire, Python<br>Ex 5 : piles, Python
        [2021 - Centres étrangers - Jour 2](++sujets/term/CentresEtrangers_juin_2021_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/CentresEtrangers_juin_2021_sujet_2_corrigé_pdf)|Ex 1 : piles, Python (mélange d'une liste)<br>Ex 2 : tuples, listes, Python (labyrinthe)<br>Ex 3 : conversion décimal/binaire, table de vérité, codage des caractères (cryptage avec XOR)<br>Ex 4 : bases de données (club de handball)<br>Ex 5 : POO (bandeau à LED) 
        [2021 - Amérique du Nord](++sujets/term/Amerique_Nord_mars_2021_pdf)|[:material-alpha-c-circle:](++sujets/term/Amerique_Nord_mars_2021_corrigé_pdf)|Ex 1 : bases de données (parc informatique)<br>Ex 2 : routage, processus, SOC (constructeur automobile)<br>Ex 3 : tableaux, Python (calcul de réductions)<br>Ex 4 : arbres binaires (compétition de badminton)<br>Ex 5 : piles, files, Python 
        [2021 - Métropole Libres - Jour 1](++sujets/term/Métropole_juin_2021_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_juin_2021_sujet_1_corrigé_pdf)|Ex 1 : bases de données (théâtre)<br>Ex 2 : piles, POO, Python<br>Ex 3 : processus, RIP, OSPF<br>Ex 4 : tableaux, Python (labyrinthe)<br>Ex 5 : tableaux, récursivité, diviser pour régner (inversions dans un tableau) 
        [2021 - Métropole Libres - Jour 2](++sujets/term/Métropole_juin_2021_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_juin_2021_sujet_2_corrigé_pdf)|Ex 1 : bases de données (bibliothèque)<br>Ex 2 : processus<br>Ex 3 : arbres binaires de recherche, POO<br>Ex 4 : récursivité, Python (mélange d'une liste)<br>Ex 5 : tableaux, Python 
        [2021 - Métropole - Jour 1](++sujets/term/Métropole_mars_2021_sujet_1_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_mars_2021_sujet_1_corrigé_pdf)|Ex 1 : arbres binaires de recherche, POO<br>Ex 2 : processus, opérateurs booléens<br>Ex 3 : bases de données (trains)<br>Ex 4 : tri fusion, diviser pour régner, Python<br>Ex 5 : réseaux, protocoles de routage 
        [2021 - Métropole - Jour 2](++sujets/term/Métropole_mars_2021_sujet_2_pdf)|[:material-alpha-c-circle:](++sujets/term/Métropole_mars_2021_sujet_2_corrigé_pdf)|Ex 1 : arbres, POO<br>Ex 2 : bases de données (restaurant)<br>Ex 3 : réseaux, protocoles de routage<br>Ex 4 : systèmes d'exploitation, processus<br>Ex 5 : files, Python 
        [2021 - Métropole Septembre Sujet 1](++sujets/term/21-NSIJ1ME3_pdf)|[:material-alpha-c-circle:](++sujets/term/21-NSIJ1ME3-corrige_pdf) | Ex 1 : protocoles de communication, architecture réseau, routage <br> Ex 2 : recherche dichotomique, récursivité <br> Ex 3 : bases de données (AirOne) <br> Ex 4 : programmation objet, spécification <br> Ex 5 : arbre, arbre binaire, pile, Python
        [2021 - Métropole Septembre Sujet 2](++sujets/term/21-NSIJ2ME3_pdf)|[:material-alpha-c-circle:](++sujets/term/21-NSIJ2ME3-corrige_pdf) | Ex 1 : protocoles de communication, réseaux, protocoles de routage <br> Ex 2 : tableaux, dictionnaires, Python <br> Ex 3 : bases de données (tableau périodique) <br> Ex 4 : piles, POO, Python <br> Ex 5 : traitement des données en tables, Python 
        [2021 - Polynésie Sujet 2](++sujets/term/21-NSIJ2PO1_pdf)|[:material-alpha-c-circle:](++sujets/term/21-NSIJ2PO1-corrige_pdf) | Ex 1 : algorithmes de tri, Python <br> Ex 2 : bases de données (vente en ligne) <br> Ex 3 : arbres binaires de recherche, POO <br> Ex 4 : architectures matérielles, systèmes d'exploitation, routage <br> Ex 5 : bases de données (vidéo à la demande)
        [2021 - Sujet Zéro](++sujets/term/21-NSIZERO_pdf)|| Ex 1 : piles, Python <br> Ex 2 : récursivité, Python (chemin dans un tableau) <br> Ex 3 : arbres binaires, arbres binaire de recherche, Python <br> Ex 4 : bases de données (lycée) <br> Ex 5 : réseaux, RIP, OSPF (routeurs)








    === "Épreuves pratiques (1h)"
        !!! warning "Pas pour les candidat·e·s libres"

        ??? example "Exercice 01.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/01_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/01_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 01.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/01_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/01_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-01.py){. target="_blank"}
            
        ??? example "Exercice 02.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/02_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/02_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 02.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/02_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/02_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-02.py){. target="_blank"}
            
        ??? example "Exercice 03.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/03_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/03_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 03.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/03_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/03_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-03.py){. target="_blank"}
            

        ??? example "Exercice 04.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/04_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/04_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 04.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/04_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/04_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-04.py){. target="_blank"}
            

        ??? example "Exercice 05.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/05_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/05_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 05.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/05_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/05_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-05.py){. target="_blank"}
            
        ??? example "Exercice 06.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/06_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/06_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 06.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/06_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/06_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-06.py){. target="_blank"}
            
        ??? example "Exercice 07.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/07_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/07_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 07.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/07_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/07_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-07.py){. target="_blank"}

        ??? example "Exercice 08.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/08_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/08_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 08.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/08_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/08_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-08.py){. target="_blank"}
            
        ??? example "Exercice 09.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/09_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/09_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 09.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/09_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/09_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-09.py){. target="_blank"}

        ??? example "Exercice 10.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/10_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/10_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 10.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/10_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/10_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-10.py){. target="_blank"}
            
        ??? example "Exercice 11.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/11_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/11_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 11.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/11_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/11_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-11.py){. target="_blank"}
            
        ??? example "Exercice 12.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/12_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/12_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 12.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/12_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/12_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-12.py){. target="_blank"}
            
        ??? example "Exercice 13.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/13_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/13_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 13.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/13_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/13_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-13.py){. target="_blank"}
            
        ??? example "Exercice 14.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/14_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/14_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 14.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/14_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/14_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-14.py){. target="_blank"}
            
        ??? example "Exercice 15.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/15_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/15_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 15.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/15_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/15_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-15.py){. target="_blank"}
            
        ??? example "Exercice 16.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/16_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/16_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 16.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/16_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/16_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-16.py){. target="_blank"}
            
        ??? example "Exercice 17.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/17_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/17_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 17.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/17_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/17_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-17.py){. target="_blank"}
            
        ??? example "Exercice 18.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/18_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/18_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 18.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/18_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/18_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-18.py){. target="_blank"}
            
        ??? example "Exercice 19.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/19_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/19_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 19.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/19_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/19_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-19.py){. target="_blank"}

        ??? example "Exercice 20.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/20_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/20_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 20.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/20_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/20_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-20.py){. target="_blank"}
            
        ??? example "Exercice 21.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/21_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/21_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 21.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/21_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/21_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-21.py){. target="_blank"}
            
        ??? example "Exercice 22.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/22_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/22_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 22.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/22_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/22_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-22.py){. target="_blank"}
        
        ??? example "Exercice 23.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/23_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/23_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 23.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/23_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/23_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-23.py){. target="_blank"}

        ??? example "Exercice 24.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/24_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/24_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 24.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/24_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/24_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-24.py){. target="_blank"}
            
        ??? example "Exercice 25.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/25_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/25_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 25.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/25_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/25_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-25.py){. target="_blank"}
            
        ??? example "Exercice 26.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/26_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/26_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 26.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/26_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/26_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-26.py){. target="_blank"}
            
        ??? example "Exercice 27.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/27_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/27_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 27.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/27_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/27_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-27.py){. target="_blank"}
            
        ??? example "Exercice 28.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/28_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/28_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 28.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/28_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/28_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-28.py){. target="_blank"}

        ??? example "Exercice 29.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/29_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/29_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 29.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/29_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/29_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-29.py){. target="_blank"}

        ??? example "Exercice 30.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/30_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/30_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 30.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/30_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/30_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-30.py){. target="_blank"}
            
        ??? example "Exercice 31.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/31_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/31_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 31.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/31_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/31_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-31.py){. target="_blank"}
            
        ??? example "Exercice 32.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/32_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/32_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 32.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/32_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/32_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-32.py){. target="_blank"}
            
        ??? example "Exercice 33.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/33_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/33_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 33.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/33_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/33_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-33.py){. target="_blank"}
            
        ??? example "Exercice 34.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/34_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/34_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 34.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/34_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/34_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-34.py){. target="_blank"}
            
        ??? example "Exercice 35.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/35_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/35_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 35.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/35_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/35_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-35.py){. target="_blank"}
            
        ??? example "Exercice 36.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/36_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/36_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 36.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/36_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/36_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-36.py){. target="_blank"}
            
        ??? example "Exercice 37.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/37_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/37_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 37.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/37_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/37_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-37.py){. target="_blank"}
            
        ??? example "Exercice 38.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/38_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/38_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 38.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/38_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/38_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-38.py){. target="_blank"}
            
        ??? example "Exercice 39.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/39_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/39_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 39.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/39_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/39_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-39.py){. target="_blank"}
        
        ??? example "Exercice 40.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/40_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/40_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 40.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/40_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/40_2/correction.md"
            
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-40.py){. target="_blank"}
            
        ??? example "Exercice 41.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/41_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/41_1/correction.md"


            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 41.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/41_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/41_2/correction.md"


            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-41.py){. target="_blank"}
            
        ??? example "Exercice 42.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/42_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/42_1/correction.md"


            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 42.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/42_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/42_2/correction.md"

            
                
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-42.py){. target="_blank"}
            
        ??? example "Exercice 43.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/43_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/43_1/correction.md"


            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 43.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/43_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/43_2/correction.md"

        
                
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-43.py){. target="_blank"}
            
        ??? example "Exercice 44.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/44_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/44_1/correction.md"

            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}


        ??? example "Exercice 44.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/44_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/44_2/correction.md"

            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-44.py){. target="_blank"}
            
        ??? example "Exercice 45.1"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/45_1/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/45_1/correction.md"

            
            :arrow_right: [Rédigez votre code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/vide.py){. target="_blank"}

        ??? example "Exercice 45.2"
            === "Énoncé" 
                --8<-- "docs/assets/sujets/term/pratique/45_2/enonce.md"

            === "Correction"
                --8<-- "docs/assets/sujets/term/pratique/45_2/correction.md"
                
            :arrow_right: [Complétez le code sur Basthon](https://console.basthon.fr/?from=https://raw.githubusercontent.com/Lycee-Experimental/Lycee-Experimental.github.io/master/docs/assets/sujets/term/pratique/scripts/23-NSI-45.py){. target="_blank"}




??? note "Sujets et corrigés : autres ressources"
    Un site centralise sujets et corrigés : c'est [ici](https://kxs.fr/sujets/)

    Les sujets Term sont également corrigés en ligne [ici](https://e-nsi.forge.aeif.fr/ecrit)