---
hide:
  - toc
---
# Quizdown

## Des quiz pour s'entraîner {: #quizz }

!!! note "Remerciements" 
    Les quiz ci-dessous sont réalisés avec **QuizDown-JS**[:material-git:](https://github.com/bonartm/quizdown-js) à partir de la base de donnée de [GeNumSi](https://genumsi.inria.fr/accueil.php). 


=== "Premières"
    - [Histoire de l'informatique (Première)](Histoire_de_l_informatique_Premiere)
    - [Architectures matérielles et systèmes d’exploitation (Première)](Architectures_materielles_et_systemes_d_exploitation_Premiere)
    - [Langages et programmation (Première)](Langages_et_programmation_Premiere)
    - [Représentation de données : Types et valeurs de base (Première)](Representation_de_donnees_Types_et_valeurs_de_base_Premiere)
    - [Représentation de données : Types construits (Première)](Representation_de_donnees_Types_construits_Premiere)
    - [Traitement de données en tables (Première)](Traitement_de_donnees_en_tables_Premiere)
    - [Algorithmique (Première)](Algorithmique_Premiere)
    - [Interactions entre l’homme et la machine sur le Web (Première)](Interactions_entre_l_homme_et_la_machine_sur_le_Web_Premiere)

=== "Terminales"
    - [Histoire de l'informatique (Terminale)](Histoire_de_l_informatique_Terminale)
    - [Architectures matérielles, systèmes d’exploitation et réseaux (Terminale)](Architectures_materielles_systemes_d_exploitation_et_reseaux_Terminale)
    - [Langages et programmation (Terminale)](Langages_et_programmation_Terminale)
    - [Structures de données (Terminale)](Structures_de_donnees_Terminale)
    - [Bases de données (Terminale)](Bases_de_donnees_Terminale)
    - [Algorithmique (Terminale)](Algorithmique_Terminale)





