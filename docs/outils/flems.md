---
hide:
  - toc
---
# Flems pour le développement Web

[:simple-github:](https://github.com/porsager/flems)

<div class="flems" style="height: 400px"></div>
<style>
    textarea#input { color: black}
</style>
<script src="https://flems.io/flems.html" type="text/javascript" charset="utf-8"></script>
<script>
const flems = Flems(document.querySelector('.flems'), {
    theme           : 'material',
    files: [{
        name: 'index.html',
        content: '<h1> Hello World ! </h1>'
    },
    {
        name: 'style.css',
        content: 'h1 {color: red}'
    },
    {
        name: 'script.js',
        content: `const h1Element = document.querySelector('h1');
function blink() {
  h1Element.style.visibility = (h1Element.style.visibility === 'hidden') ? 'visible' : 'hidden';
}
setInterval(blink, 500);`
//    }],
//    links: [{
//        name: 'mithril',
//        type: 'script',
//        url: 'https://unpkg.com/mithril'
    }]
})
</script>