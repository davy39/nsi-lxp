---
hide:
  - toc
---
# Basthon

## Un notebook intégré à InformatiXP


<iframe src="/assets/tools/basthon/index.html" frameborder="0" scrolling="auto" style="width: 100%; height: 700px;"></iframe>
