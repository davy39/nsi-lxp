---
hide:
  - toc
---
# Blockly-games

Cliquer sur l'image ci-dessous :

[![Blockly-games](!!tools/blockly-games/index/title_png)](++tools/blockly-games/index_html)

??? info "Construire l'appli web à partir des sources"
    ```bash
    sudo apt install subversion
    git clone https://github.com/google/blockly-games
    cd blockly-games
    make deps
    make games
    ```

    L'appli est alors dans le dossier `appengine`