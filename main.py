from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin
from pyodide_mkdocs_theme.pyodide_macros.plugin.maestro_IDE import MaestroIDE
from pyodide_mkdocs_theme.pyodide_macros.macros.IDEs import Ide
from typing import Literal, Union

def define_env(env:PyodideMacrosPlugin):
    "Hook function"


    @env.macro
    def jupy(nb='') -> str:
        path='/assets/tools/basthon/index.html'
        notebook_dir='/notebooks/'
        if nb !='':
            path += f"?from={notebook_dir}{nb}"
        return f"""<a class="md-button md-button--primary" href="{path}" target="_blank"><span class="twemoji"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7.157 22.201A1.784 1.799 0 0 1 5.374 24a1.784 1.799 0 0 1-1.784-1.799 1.784 1.799 0 0 1 1.784-1.799 1.784 1.799 0 0 1 1.783 1.799zM20.582 1.427a1.415 1.427 0 0 1-1.415 1.428 1.415 1.427 0 0 1-1.416-1.428A1.415 1.427 0 0 1 19.167 0a1.415 1.427 0 0 1 1.415 1.427zM4.992 3.336A1.047 1.056 0 0 1 3.946 4.39a1.047 1.056 0 0 1-1.047-1.055A1.047 1.056 0 0 1 3.946 2.28a1.047 1.056 0 0 1 1.046 1.056zm7.336 1.517c3.769 0 7.06 1.38 8.768 3.424a9.363 9.363 0 0 0-3.393-4.547 9.238 9.238 0 0 0-5.377-1.728A9.238 9.238 0 0 0 6.95 3.73a9.363 9.363 0 0 0-3.394 4.547c1.713-2.04 5.004-3.424 8.772-3.424zm.001 13.295c-3.768 0-7.06-1.381-8.768-3.425a9.363 9.363 0 0 0 3.394 4.547A9.238 9.238 0 0 0 12.33 21a9.238 9.238 0 0 0 5.377-1.729 9.363 9.363 0 0 0 3.393-4.547c-1.712 2.044-5.003 3.425-8.772 3.425Z"></path></svg></span> Notebook <span class="twemoji"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7.157 22.201A1.784 1.799 0 0 1 5.374 24a1.784 1.799 0 0 1-1.784-1.799 1.784 1.799 0 0 1 1.784-1.799 1.784 1.799 0 0 1 1.783 1.799zM20.582 1.427a1.415 1.427 0 0 1-1.415 1.428 1.415 1.427 0 0 1-1.416-1.428A1.415 1.427 0 0 1 19.167 0a1.415 1.427 0 0 1 1.415 1.427zM4.992 3.336A1.047 1.056 0 0 1 3.946 4.39a1.047 1.056 0 0 1-1.047-1.055A1.047 1.056 0 0 1 3.946 2.28a1.047 1.056 0 0 1 1.046 1.056zm7.336 1.517c3.769 0 7.06 1.38 8.768 3.424a9.363 9.363 0 0 0-3.393-4.547 9.238 9.238 0 0 0-5.377-1.728A9.238 9.238 0 0 0 6.95 3.73a9.363 9.363 0 0 0-3.394 4.547c1.713-2.04 5.004-3.424 8.772-3.424zm.001 13.295c-3.768 0-7.06-1.381-8.768-3.425a9.363 9.363 0 0 0 3.394 4.547A9.238 9.238 0 0 0 12.33 21a9.238 9.238 0 0 0 5.377-1.729 9.363 9.363 0 0 0 3.393-4.547c-1.712 2.044-5.003 3.425-8.772 3.425Z"></path></svg></span></a>"""

    @env.macro
    def jupy_cor(nb='') -> str:
        path='/assets/tools/basthon/index.html'
        notebook_dir='/notebooks/'
        if nb !='':
            path += f"?from={notebook_dir}{nb}"
        return f"""<a class="md-button" href="{path}" target="_blank"><span class="twemoji"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7.157 22.201A1.784 1.799 0 0 1 5.374 24a1.784 1.799 0 0 1-1.784-1.799 1.784 1.799 0 0 1 1.784-1.799 1.784 1.799 0 0 1 1.783 1.799zM20.582 1.427a1.415 1.427 0 0 1-1.415 1.428 1.415 1.427 0 0 1-1.416-1.428A1.415 1.427 0 0 1 19.167 0a1.415 1.427 0 0 1 1.415 1.427zM4.992 3.336A1.047 1.056 0 0 1 3.946 4.39a1.047 1.056 0 0 1-1.047-1.055A1.047 1.056 0 0 1 3.946 2.28a1.047 1.056 0 0 1 1.046 1.056zm7.336 1.517c3.769 0 7.06 1.38 8.768 3.424a9.363 9.363 0 0 0-3.393-4.547 9.238 9.238 0 0 0-5.377-1.728A9.238 9.238 0 0 0 6.95 3.73a9.363 9.363 0 0 0-3.394 4.547c1.713-2.04 5.004-3.424 8.772-3.424zm.001 13.295c-3.768 0-7.06-1.381-8.768-3.425a9.363 9.363 0 0 0 3.394 4.547A9.238 9.238 0 0 0 12.33 21a9.238 9.238 0 0 0 5.377-1.729 9.363 9.363 0 0 0 3.393-4.547c-1.712 2.044-5.003 3.425-8.772 3.425Z"></path></svg></span> Corrigé <span class="twemoji"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7.157 22.201A1.784 1.799 0 0 1 5.374 24a1.784 1.799 0 0 1-1.784-1.799 1.784 1.799 0 0 1 1.784-1.799 1.784 1.799 0 0 1 1.783 1.799zM20.582 1.427a1.415 1.427 0 0 1-1.415 1.428 1.415 1.427 0 0 1-1.416-1.428A1.415 1.427 0 0 1 19.167 0a1.415 1.427 0 0 1 1.415 1.427zM4.992 3.336A1.047 1.056 0 0 1 3.946 4.39a1.047 1.056 0 0 1-1.047-1.055A1.047 1.056 0 0 1 3.946 2.28a1.047 1.056 0 0 1 1.046 1.056zm7.336 1.517c3.769 0 7.06 1.38 8.768 3.424a9.363 9.363 0 0 0-3.393-4.547 9.238 9.238 0 0 0-5.377-1.728A9.238 9.238 0 0 0 6.95 3.73a9.363 9.363 0 0 0-3.394 4.547c1.713-2.04 5.004-3.424 8.772-3.424zm.001 13.295c-3.768 0-7.06-1.381-8.768-3.425a9.363 9.363 0 0 0 3.394 4.547A9.238 9.238 0 0 0 12.33 21a9.238 9.238 0 0 0 5.377-1.729 9.363 9.363 0 0 0 3.393-4.547c-1.712 2.044-5.003 3.425-8.772 3.425Z"></path></svg></span></a>"""



    @env.macro
    def plan():
        print(env.variables.page)
        parent_page = env.variables.page.parent
        child_pages = parent_page.children if parent_page else []
        if child_pages:
            toc_html = '<div class="toc">\n<ul>\n'
            i=0
            for child_page in child_pages:
                if child_page != env.variables.page:
                    i+=1
                    if child_page.children:
                        toc_html += f'<li>Chapitre {i} : {child_page.title}</li>\n<ul>\n'
                        for child in child_page.children:
                            if child != child_page:
                                toc_html += f'<li><a class="toc" href="/{child.url}">{child.title}</a></li>\n'           
                        toc_html += '</ul>'
                    else:
                        toc_html += f'<li><a class="toc" href="/{child_page.url}">Chapitre {i} : {child_page.title}</a></li>\n'           
            toc_html += '</ul>\n</div>'
#                    toc_html += f'<li><a href="/{child_page.url}">Chapitre {i}: {child_page.title}</a></li>\n'           

        
        return toc_html


    @env.macro
    def blockly() -> str:
        return """<script src="https://unpkg.com/blockly"></script>
<script src="https://unpkg.com/blockly/blocks"></script>
<script src="https://unpkg.com/blockly/python_compressed"></script>
<script src="https://unpkg.com/blockly/msg/fr"></script>
<script src="https://unpkg.com/@blockly/theme-dark"></script>
<div id="blocklyDiv" style="height: 600px; width: 100%"></div>
<script type="text/javascript">
    var toolbox = {
'kind': 'categoryToolbox',
'contents': [
{
    'kind': 'category',
    'name': 'Logique',
    'categorystyle': 'logic_category',
    'contents': [
    {
        'type': 'controls_if',
        'kind': 'block',
    },
    {
        'type': 'logic_compare',
        'kind': 'block',
        'fields': {
        'OP': 'EQ',
        },
    },
    {
        'type': 'logic_operation',
        'kind': 'block',
        'fields': {
        'OP': 'AND',
        },
    },
    {
        'type': 'logic_negate',
        'kind': 'block',
    },
    {
        'type': 'logic_boolean',
        'kind': 'block',
        'fields': {
        'BOOL': 'TRUE',
        },
    },
    {
        'type': 'logic_null',
        'kind': 'block',
        'enabled': false,
    },
    {
        'type': 'logic_ternary',
        'kind': 'block',
    },
    ],
},
{
    'kind': 'category',
    'name': 'Boucles',
    'categorystyle': 'loop_category',
    'contents': [
    {
        'type': 'controls_repeat_ext',
        'kind': 'block',
        'inputs': {
        'TIMES': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 10,
            },
            },
        },
        },
    },
    {
        'type': 'controls_repeat',
        'kind': 'block',
        'enabled': false,
        'fields': {
        'TIMES': 10,
        },
    },
    {
        'type': 'controls_whileUntil',
        'kind': 'block',
        'fields': {
        'MODE': 'WHILE',
        },
    },
    {
        'type': 'controls_for',
        'kind': 'block',
        'fields': {
        'VAR': {
            'name': 'i',
        },
        },
        'inputs': {
        'FROM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        'TO': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 10,
            },
            },
        },
        'BY': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        },
    },
    {
        'type': 'controls_forEach',
        'kind': 'block',
        'fields': {
        'VAR': {
            'name': 'j',
        },
        },
    },
    {
        'type': 'controls_flow_statements',
        'kind': 'block',
        'enabled': false,
        'fields': {
        'FLOW': 'BREAK',
        },
    },
    ],
},
{
    'kind': 'category',
    'name': 'Maths',
    'categorystyle': 'math_category',
    'contents': [
    {
        'type': 'math_number',
        'kind': 'block',
        'fields': {
        'NUM': 123,
        },
    },
    {
        'type': 'math_arithmetic',
        'kind': 'block',
        'fields': {
        'OP': 'ADD',
        },
        'inputs': {
        'A': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        'B': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        },
    },
    {
        'type': 'math_single',
        'kind': 'block',
        'fields': {
        'OP': 'ROOT',
        },
        'inputs': {
        'NUM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 9,
            },
            },
        },
        },
    },
    {
        'type': 'math_trig',
        'kind': 'block',
        'fields': {
        'OP': 'SIN',
        },
        'inputs': {
        'NUM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 45,
            },
            },
        },
        },
    },
    {
        'type': 'math_constant',
        'kind': 'block',
        'fields': {
        'CONSTANT': 'PI',
        },
    },
    {
        'type': 'math_number_property',
        'kind': 'block',
        'fields': {
        'PROPERTY': 'EVEN',
        },
        'inputs': {
        'NUMBER_TO_CHECK': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 0,
            },
            },
        },
        },
    },
    {
        'type': 'math_round',
        'kind': 'block',
        'fields': {
        'OP': 'ROUND',
        },
        'inputs': {
        'NUM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 3.1,
            },
            },
        },
        },
    },
    {
        'type': 'math_on_list',
        'kind': 'block',
        'fields': {
        'OP': 'SUM',
        },
    },
    {
        'type': 'math_modulo',
        'kind': 'block',
        'inputs': {
        'DIVIDEND': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 64,
            },
            },
        },
        'DIVISOR': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 10,
            },
            },
        },
        },
    },
    {
        'type': 'math_constrain',
        'kind': 'block',
        'inputs': {
        'VALUE': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 50,
            },
            },
        },
        'LOW': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        'HIGH': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 100,
            },
            },
        },
        },
    },
    {
        'type': 'math_random_int',
        'kind': 'block',
        'inputs': {
        'FROM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        'TO': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 100,
            },
            },
        },
        },
    },
    {
        'type': 'math_random_float',
        'kind': 'block',
    },
    {
        'type': 'math_atan2',
        'kind': 'block',
        'inputs': {
        'X': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        'Y': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 1,
            },
            },
        },
        },
    },
    ],
},
{
    'kind': 'category',
    'name': 'Texte',
    'categorystyle': 'text_category',
    'contents': [
    {
        'type': 'text',
        'kind': 'block',
        'fields': {
        'TEXT': '',
        },
    },
    {
        'type': 'text_multiline',
        'kind': 'block',
        'fields': {
        'TEXT': '',
        },
    },
    {
        'type': 'text_join',
        'kind': 'block',
    },
    {
        'type': 'text_append',
        'kind': 'block',
        'fields': {
        'name': 'item',
        },
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        },
    },
    {
        'type': 'text_length',
        'kind': 'block',
        'inputs': {
        'VALUE': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    {
        'type': 'text_isEmpty',
        'kind': 'block',
        'inputs': {
        'VALUE': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        },
    },
    {
        'type': 'text_indexOf',
        'kind': 'block',
        'fields': {
        'END': 'FIRST',
        },
        'inputs': {
        'VALUE': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'text',
                },
            },
            },
        },
        'FIND': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    {
        'type': 'text_charAt',
        'kind': 'block',
        'fields': {
        'WHERE': 'FROM_START',
        },
        'inputs': {
        'VALUE': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'text',
                },
            },
            },
        },
        },
    },
    {
        'type': 'text_getSubstring',
        'kind': 'block',
        'fields': {
        'WHERE1': 'FROM_START',
        'WHERE2': 'FROM_START',
        },
        'inputs': {
        'STRING': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'text',
                },
            },
            },
        },
        },
    },
    {
        'type': 'text_changeCase',
        'kind': 'block',
        'fields': {
        'CASE': 'UPPERCASE',
        },
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    {
        'type': 'text_trim',
        'kind': 'block',
        'fields': {
        'MODE': 'BOTH',
        },
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    {
        'type': 'text_count',
        'kind': 'block',
        'inputs': {
        'SUB': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        },
    },
    {
        'type': 'text_replace',
        'kind': 'block',
        'inputs': {
        'FROM': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        'TO': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        },
    },
    {
        'type': 'text_reverse',
        'kind': 'block',
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': '',
            },
            },
        },
        },
    },
    {
        'type': 'text_print',
        'kind': 'block',
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    {
        'type': 'text_prompt_ext',
        'kind': 'block',
        'fields': {
        'TYPE': 'TEXT',
        },
        'inputs': {
        'TEXT': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': 'abc',
            },
            },
        },
        },
    },
    ],
},
{
    'kind': 'category',
    'name': 'Listes',
    'categorystyle': 'list_category',
    'contents': [
    {
        'type': 'lists_create_with',
        'kind': 'block',
    },
    {
        'type': 'lists_create_with',
        'kind': 'block',
    },
    {
        'type': 'lists_repeat',
        'kind': 'block',
        'inputs': {
        'NUM': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 5,
            },
            },
        },
        },
    },
    {
        'type': 'lists_length',
        'kind': 'block',
    },
    {
        'type': 'lists_isEmpty',
        'kind': 'block',
    },
    {
        'type': 'lists_indexOf',
        'kind': 'block',
        'fields': {
        'END': 'FIRST',
        },
        'inputs': {
        'VALUE': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'list',
                },
            },
            },
        },
        },
    },
    {
        'type': 'lists_getIndex',
        'kind': 'block',
        'fields': {
        'MODE': 'GET',
        'WHERE': 'FROM_START',
        },
        'inputs': {
        'VALUE': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'list',
                },
            },
            },
        },
        },
    },
    {
        'type': 'lists_setIndex',
        'kind': 'block',
        'fields': {
        'MODE': 'SET',
        'WHERE': 'FROM_START',
        },
        'inputs': {
        'LIST': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'list',
                },
            },
            },
        },
        },
    },
    {
        'type': 'lists_getSublist',
        'kind': 'block',
        'fields': {
        'WHERE1': 'FROM_START',
        'WHERE2': 'FROM_START',
        },
        'inputs': {
        'LIST': {
            'block': {
            'type': 'variables_get',
            'fields': {
                'VAR': {
                'name': 'list',
                },
            },
            },
        },
        },
    },
    {
        'type': 'lists_split',
        'kind': 'block',
        'fields': {
        'MODE': 'SPLIT',
        },
        'inputs': {
        'DELIM': {
            'shadow': {
            'type': 'text',
            'fields': {
                'TEXT': ',',
            },
            },
        },
        },
    },
    {
        'type': 'lists_sort',
        'kind': 'block',
        'fields': {
        'TYPE': 'NUMERIC',
        'DIRECTION': '1',
        },
    },
    {
        'type': 'lists_reverse',
        'kind': 'block',
    },
    ],
},
{
    'kind': 'category',
    'categorystyle': 'colour_category',
    'name': 'Couleur',
    'contents': [
    {
        'type': 'colour_picker',
        'kind': 'block',
        'fields': {
        'COLOUR': '#ff0000',
        },
    },
    {
        'type': 'colour_random',
        'kind': 'block',
    },
    {
        'type': 'colour_rgb',
        'kind': 'block',
        'inputs': {
        'RED': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 100,
            },
            },
        },
        'GREEN': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 50,
            },
            },
        },
        'BLUE': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 0,
            },
            },
        },
        },
    },
    {
        'type': 'colour_blend',
        'kind': 'block',
        'inputs': {
        'COLOUR1': {
            'shadow': {
            'type': 'colour_picker',
            'fields': {
                'COLOUR': '#ff0000',
            },
            },
        },
        'COLOUR2': {
            'shadow': {
            'type': 'colour_picker',
            'fields': {
                'COLOUR': '#3333ff',
            },
            },
        },
        'RATIO': {
            'shadow': {
            'type': 'math_number',
            'fields': {
                'NUM': 0.5,
            },
            },
        },
        },
    },
    ],
},
{
    'kind': 'sep',
},
{
    'kind': 'category',
    'name': 'Variables',
    'custom': 'VARIABLE',
    'categorystyle': 'variable_category',
},
{
    'kind': 'category',
    'name': 'Fonctions',
    'custom': 'PROCEDURE',
    'categorystyle': 'procedure_category',
},

],
};
var workspace = Blockly.inject("blocklyDiv", { toolbox: toolbox, theme: 'dark' });
function myUpdateFunction(event) {
var code = python.pythonGenerator.workspaceToCode(workspace);
// Get a reference to the Ace editor element
const editorDiv = document.querySelector('div[id^="editor_"]');
editor = ace.edit(editorDiv.id);
// Set the value of the editor to some code
editor.setValue(code, -1);
}
workspace.addChangeListener(myUpdateFunction);
</script>"""

    @env.macro
    def exo(titre, prem=None) -> str:
        # si var == False, alors l'exercice est placé dans une superfence.
        if prem is not None:
            env.variables["compteur_exo"] = prem
        else:
            env.variables["compteur_exo"] += 1
        return f"""??? question "Activité { env.variables['compteur_exo']} - {titre}"  """

    @env.macro
    def tube_sci(titre, embed) -> str:
        return f"""<div class="admonition video" bis_skin_checked="1">\
<p class="admonition-title">{titre}</p>\
<iframe title="{titre}"\
    width="700" height="394" style="display:block;margin: auto;"\
    src="https://tube-sciences-technologies.apps.education.fr/videos/embed/{embed}"\
    frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups">\
</iframe>\
</div>"""
  
    @env.macro
    def youtube(titre, embed) -> str:
        return f"""<div class="admonition video" bis_skin_checked="1">\
<p class="admonition-title">{titre}</p>\
<iframe sandbox="allow-scripts allow-same-origin" src="https://ladigitale.dev/digiview/inc/video.php?videoId={embed}&vignette=https://i.ytimg.com/vi/{embed}/hqdefault.jpg&debut=0&fin=22486&largeur=16&hauteur=9" allowfullscreen frameborder="0" width="700" height="394" style="display:block;margin: auto;"></iframe></div>"""

    @env.macro
    def quiz(source, nb=10) -> str:
        return f"""!!! tip "Recharger la page pour de nouvelles questions."
<div class="quizdown">
---
primaryColor: "var(--md-primary-fg-color)"
shuffleQuestions: true
shuffleAnswers: true
nQuestions: {nb}
--- 
--8<-- "docs/assets/tools/quizdown/sources/{source}.md"
</div>
<script src="/assets/tools/quizdown/quizdown.js"></script>
<script src="/assets/tools/quizdown/quizdownHighlight.js"></script>
<script src="/assets/tools/quizdown/quizdownKatex.js"></script> 
<script>
window.onload = () => {{
  quizdown.register(quizdownHighlight).init();
}};</script>
"""

    @env.macro
    def alpine() -> str:
        return """<iframe\
  id="inlineFrameExample"\
  title="Inline Frame Example"\
  width="100%"\
  height="700"\
  src="https://v86.codeberg.page/?profile=alpine">\
</iframe>"""

    @env.macro
    def debian_back() -> str:
        return '''<div id="screen_container">\
        <div style="white-space: pre; font: 14px monospace; line-height: 14px"></div>\
        <canvas tabindex="0" style="display: none"></canvas></div>\
        <script src="/assets/tools/v86/libv86.js"></script>
<script>
"use strict";
function launchdebian() {
    var emulator = new V86Starter({
        wasm_path: "/assets/tools/v86/v86.wasm",
        memory_size: 512 * 1024 * 1024,
        vga_memory_size: 8 * 1024 * 1024,
        screen_container: document.getElementById("screen_container"),
        <!-- initial_state: { url: "/assets/tools/v86/debian-state-base.bin.zst" }, -->
        initial_state: { url: "https://raw.githubusercontent.com/davy39/v86/master/images/debian-state-base.bin.zst" },
        filesystem: { baseurl: "https://raw.githubusercontent.com/davy39/v86/master/images/debian-9p-rootfs-flat/" },
        <!-- filesystem: { baseurl: "/assets/tools/v86/debian-9p-rootfs-flat/" },-->
        network_relay_url: "wss://relay.widgetry.org/",
        autostart: true,
    });
}
// Function to be executed when the `open` attribute changes
function handleOpenAttributeChange(mutationsList) {
  for (let mutation of mutationsList) {
    if (mutation.type === 'attributes' && mutation.attributeName === 'open') {
      const isOpen = mutation.target.hasAttribute('open');
      if (isOpen) {
        setTimeout(function() {
          launchalpine();
          console.log('<details.debian> element opened:', mutation.target);
        }, 100);
      } else {
        console.log('<details.debian> element closed:', mutation.target);
      }
    }
  }
}

// Select the target node(s) using a CSS selector
const debiansElements = document.querySelectorAll('details.debian');

// Create a new MutationObserver instance
const observer = new MutationObserver(handleOpenAttributeChange);

// Configure the observer to watch for changes in the `open` attribute
const observerConfig = {
  attributes: true,
  attributeFilter: ['open']
};

// Start observing the target node(s) and listen for attribute changes
debiansElements.forEach(element => {
  observer.observe(element, observerConfig);
});
</script>


'''